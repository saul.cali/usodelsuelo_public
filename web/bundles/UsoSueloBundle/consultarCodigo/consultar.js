saul.config(["$stateProvider",
	function ($stateProvider) {
		$stateProvider.state("consultarCodigo", {
			url: "/usosuelo/consultarcodigo/{codigo}",
			views: {
				"main": {
					controller: "consultarCodigoCtrl",
					templateUrl: "bundles/UsoSueloBundle/consultarCodigo/consultar.tpl.html"
				}
			}
		});
	}
]).controller("consultarCodigoCtrl", ["$scope", "$stateParams", "ConsultarCodigoConsultaUsosuelo", "$timeout", "$state",
	function ($scope, $stateParams, ConsultarCodigoConsultaUsosuelo, $timeout, $state)
	{

		$scope.start = function () {
			$scope.init();
		};


		$scope.init = function () {
			//$scope.initInputFile();
			$scope.messageErrorConsulta = '';
			$scope.Registroconsultausosuelo.codigo = '';
			$scope.Registroconsultausosuelo = {};
		};

		$scope.cancel = function () {
		};

		$scope.limpiarCodigo = function () {
			$scope.start();
		};

		$scope.buscarCodigo = function () {

			$(".overlap_espera").show();
			$(".overlap_espera_1").show();
			$scope.messageErrorConsulta = "";
			
			if ($scope.Registroconsultausosuelo != null) {
				ConsultarCodigoConsultaUsosuelo.get({
					codigo: $scope.Registroconsultausosuelo.codigo
				}).$promise.then(
						function (response) {
							$(".overlap_espera").fadeOut(500, "linear");
							$(".overlap_espera_1").fadeOut(500, "linear");

							if (response.success) {
								$scope.messageErrorConsulta = "";
								$scope.Registroconsultausosuelo = response.data;
							} else {								
								$scope.messageErrorConsulta = "No se encontró ningun registro asociado al codigo " + $scope.Registroconsultausosuelo.codigo + ", por favor vuelva a intentarlo.";
								$scope.Registroconsultausosuelo = {codigo:$scope.Registroconsultausosuelo.codigo};
							}
						},
						function (error) {
							$(".overlap_espera").fadeOut(500, "linear");
							$(".overlap_espera_1").fadeOut(500, "linear");
							$scope.Registroconsultausosuelo = {};
							$scope.messageErrorConsulta = "Ha ocurrido un error, por favor vuelva a intentarlo.";
							//console.error(error);
						}
				);
			} else {
				$(".overlap_espera").fadeOut(500, "linear");
				$(".overlap_espera_1").fadeOut(500, "linear");
				$scope.Registroconsultausosuelo = {};
				$scope.messageErrorConsulta = "Ha ocurrido un error, por favor vuelva a intentarlo.";
			}

		};
	}
]);
