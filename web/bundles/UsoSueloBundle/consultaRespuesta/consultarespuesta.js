saul.config(["$stateProvider",
	function ($stateProvider) {
		$stateProvider.state("consultaRespuesta", {
			url: "/usosuelo/consultarespuesta",
			views: {
				main: {
					controller: "consultaRespuestaCtrl",
					templateUrl: "bundles/UsoSueloBundle/consultaRespuesta/consultarespuesta.tpl.html"
				}
			}
		})
				.state("ListadoActividadesCiiu", {
					url: "/usosuelo/consultarActividadesCiiu",
					views: {
						"main": {
							controller: "ListarActividadesCiiuCtrl",
							templateUrl: "bundles/UsoSueloBundle/consultaRespuesta/consultarespuesta.tpl.html"
						}
					}
				});
	}])
		.directive('tagManager2', [function () {
				return {
					restrict: 'E',
					scope: {
						tags: '=',
						autocomplete: '=autocomplete',
						request: '=',
						codigo: '=',
						fecha: '='
					},
					template:
							'<div class="tags" ng-disabled="solicitud.idSolicitud">' +
							'<div ng-disabled="solicitud.idSolicitud" ng-repeat="(idx, tag) in tags" class="tag"><span class="label label-info alert-info">{{ tag.codCiiu +" - "+tag.nombreCiiu }} <a class="close" href ng-click="remove(idx)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Actividad {{ tag.codCiiu }}">×</a></span></div>' +
							'</div>' +
							'<div class="input-group"><input type="text" id="buscar" class="form-control" placeholder="Por favor escriba la actividad que desea buscar" ng-disabled="solicitud.idSolicitud" ng-model="newValue" /> ' +
							'<span class="input-group-btn"><a class="btn btn-default" ng-disabled="solicitud.idSolicitud" ng-click="add()" id="botonbuscar">Agregar</a></span></div>',
					link: function ($scope, $element) {

						$scope.seleccionados = [];
						var input = angular.element($element).find('input');
						// setup autocomplete
						if ($scope.autocomplete) {
							$scope.autocompleteFocus = function (event, ui) {
								input.val(ui.item.value);
								return false;
							};
							$scope.autocompleteSelect = function (event, ui) {
								$scope.newValue = ui.item.value;
								$scope.$apply($scope.add);
								return false;
							};
							$($element).find('input').autocomplete({
								minLength: 0,
								source: function (request, response) {
									var item;
									var itemMostrar;
									return response((function () {
										var _i, _len, _ref, _results;
										_ref = $scope.autocomplete;
										_results = [];
										for (_i = 0, _len = _ref.length; _i < _len; _i++) {

											item = _ref[_i].codCiiu + " - " + _ref[_i].nombreCiiu + " - " + _ref[_i].palabraClave;
											itemMostrar = _ref[_i].codCiiu + " - " + _ref[_i].nombreCiiu;										

											if (item.toLowerCase().indexOf(request.term.toLowerCase()) !== -1) {											
												_results.push(itemMostrar);
											}
										}
										return _results;
									})());
								},
								focus: (function (_this) {
									return function (event, ui) {
										return $scope.autocompleteFocus(event, ui);
									};
								})(this),
								select: (function (_this) {
									return function (event, ui) {

										var str = ui.item.value;
										var codCiiu = str.split(" - ");
										//console.warn($scope.autocomplete);
										var indice = $scope.autocomplete.findIndex(function (element) {
											//console.warn(element.codCiiu==codCiiu[0]);
											return element.codCiiu == codCiiu[0] && element.nombreCiiu == codCiiu[1];
										});
										//console.warn("Encontramos a: ", $scope.autocomplete[indice]);

										//alert(codCiiu[0]);
										//console.log(ui.item.value);
										//$scope.save(ui.item.value);
										$('#botonbuscar').attr("disabled", false);

										return $scope.autocompleteSelect(event, ui);
									};
								})(this)
							});
						}


						// adds the new tag to the array
						$scope.add = function () {
							var codCiiu = $scope.newValue.split(" - ");
							var indice = $scope.autocomplete.findIndex(function (element) {
								//console.warn(element.codCiiu==codCiiu[0]);
								if (element.observacion == "null") {
									console.warn("es nulo");
								}
								return element.codCiiu == codCiiu[0] && element.nombreCiiu == codCiiu[1];
							});
							//console.warn($scope.tags);
							//console.warn("Encontramos a: ", $scope.autocomplete[indice]);
							// if not dupe, add it
							if ($('#buscar').val() == "") {
								$('#botonbuscar').attr("disabled", true);
							}
							if ($('#buscar').val() != "") {
								//if ($scope.tags.indexOf($scope.newValue) == -1) {
								//console.warn("El nuevo valor es ...", $scope.newValue);

								if ($scope.tags.findIndex(function (element) {
									return element.codCiiu == $scope.autocomplete[indice].codCiiu && element.nombreCiiu == $scope.autocomplete[indice].nombreCiiu;
								}) == -1) {
									$scope.tags.push($scope.autocomplete[indice]);

									var str = $scope.newValue;
									var respuesta = str.split("-");

								}

								/*if ($scope.tags.indexOf($scope.newValue) == -1) {
								 $scope.tags.push($scope.newValue);
								 
								 var str = $scope.newValue;
								 var respuesta = str.split("-");
								 
								 alert(respuesta[2]);
								 }*/								
								$scope.newValue = "";
							}
						};

						// remove an item
						$scope.remove = function (idx) {
							$scope.tags.splice(idx, 1);
							$('#botonbuscar').attr("disabled", true);
						};

						// capture keypresses
						input.bind('keypress', function (event) {
							// enter was pressed
							if (event.keyCode == 13) {
								$scope.$apply($scope.add);
							}
						});
					}
				};
			}])

		.controller("consultaRespuestaCtrl", ["$scope", "$stateParams", "ConsultaRespuestaUsosuelo", "urlPlugInNomenclatura", "ListarActividadesUsoxCiiu", "ConsultarDatosPredio", "$timeout", "$state",
			function ($scope, $stateParams, ConsultaRespuestaUsosuelo, urlPlugInNomenclatura, ServicioListarActividadesCiiu, ConsultarDatosPredio, $timeout, $state) {

				$scope.ciiu = function () {
					$scope.tags = [];
					$scope.allTags = [];

					var request = {'currentPage': 1,
						'numPerPage': 1000, //aqui se carga la lista completa para hacer la busqueda
						'maxSize': 10,
						'orderBy': 'codCiiu'
					};

					$scope.request = request;

					ServicioListarActividadesCiiu
							.get(request)
							.$promise
							.then(function (response) {
								for (var i in response.alldata) {
									$scope.allTags.push(response.alldata[i]);
								}
							});
				};

				$scope.tags = [];
				$scope.allTags = [];
				$scope.data = $stateParams;
				$scope.currentPage = 1;
				$scope.numPerPage = 20;
				$scope.maxSize = 10;
				$scope.rowWarning = 'estaEnviado';
				$scope.orderBy = 'codCiiu';

				$scope.actualizarDatos = function ()
				{
					var request = {'currentPage': $scope.currentPage,
						'numPerPage': $scope.numPerPage,
						'orderBy': $scope.orderBy
					};
					for (header in $scope.headers) {
						if (typeof $scope.headers[header].value !== 'undefined' && $scope.headers[header].value !== '') {
							request[$scope.headers[header].name] = $scope.headers[header].value;
						}
					}

				};

				$scope.ciiu();
				$scope.actualizarDatos();

				$('#botonbuscar').attr("disabled", true);

				$scope.ordenarPor = function (name)
				{
					$scope.orderBy = name;
					$scope.actualizarDatos();
				}

				$scope.alerts = [];

				$scope.closeAlert = function (index) {
					$scope.alerts.splice(index, 1);
				};

				$scope.start = function () {
					$scope.init();
				};


				$scope.init = function () {
					//$scope.initInputFile();
					$scope.messageErrorConsulta = "";
					$scope.Registroconsultausosuelo = null;
					$scope.npn = "";
				};

				$scope.cancel = function () {
				};

				$scope.limpiarDatos = function () {
					$scope.start();
					$scope.tags = null;
					$scope.allTags = null;
//revisar eso tagas					
				};


				$scope.obtenerModalIngresoDireccion = function () {
					$(".overlap_espera").show();
					$(".overlap_espera_1").show();

					$.ajax({
						url: urlPlugInNomenclatura,
						data: {
							fieldNamecallbackFunction: 'cerrarModal',
							fieldNameNPNrequerido: !1,
							activarBusqueda: !0,
							fieldNameDiv: "modalIngresoDireccion",
							fieldNameNumeroUnico: "npn",
							fieldNameDireccion: "direccion",
							fieldNameDireccionVisual: "direccionVisual",
							fieldNameDireccionAdicional: "infoAdicional",
							fieldNameIdPredio: "predioId",
							fieldNameIdNomenclatura: "nomenclaturaId"
						},
						success: function (a) {
							$(".overlap_espera").fadeOut(500, "linear");
							$(".overlap_espera_1").fadeOut(500, "linear", function () {
								$("#modalIngresoDireccion").empty(), $("#modalIngresoDireccion").append(a), $("#modalIngresoDireccion").modal("show")
							});
						},
						error: function (jqXHR, textStatus, errorThrown) {
							$(".overlap_espera").fadeOut(500, "linear");
							$(".overlap_espera_1").fadeOut(500, "linear");
							switch (textStatus) {
								case 'timeout':
									alert("Se ha agotado el tiempo de respuesta del servidor. Intente nuevamente. (Error " + jqXHR.status + ")");
									break;
								case 'error':
									alert("Se ha generado un error en el servidor. Intente nuevamente o consulte al administrador. (Error " + jqXHR.status + ")");
									break;
								case 'abort':
									alert("El servidor ha abortado la conexión. Intente nuevamente. (Error " + jqXHR.status + ")");
									break;
								case 'parsererror':
									alert("La respuesta del servidor no ha sido correcta. Consulte al administrador. (Error " + jqXHR.status + ")");
									break;
								default:
									alert("Lo sentimos, ha ocurrido un error. Intente nuevamente. (Error " + jqXHR.status + ")");
									break;
							}
						}
					});
				};

				cerrarModal = function () {
					$('#modalIngresoDireccion').modal('toggle');

					if (
							!(document.getElementById('npn').value == ''
									&& document.getElementById('direccion').value == ''
									&& document.getElementById('predioId').value == ''
									&& document.getElementById('nomenclaturaId').value == '')
							) {
						$scope.npn = document.getElementById('npn').value;
						$scope.direccion = document.getElementById('direccion').value + document.getElementById('infoAdicional').value;
						$scope.idpredio = document.getElementById('predioId').value != '' ? document.getElementById('predioId').value : null;
						$scope.idnomenclatura = document.getElementById('nomenclaturaId').value != '' ? document.getElementById('nomenclaturaId').value : null;

						document.getElementById('npn').value = '';
						document.getElementById('direccion').value = '';
						document.getElementById('infoAdicional').value = '';
						document.getElementById('predioId').value = '';
						document.getElementById('nomenclaturaId').value = '';

					}

				};

				$scope.buscarDatos = function () {

					$(".overlap_espera").show();
					$(".overlap_espera_1").show();
					$scope.messageErrorConsulta = "";

					var elementos = document.getElementsByName("idciiu");
					var numactividades = elementos.length;
					var actividades = [];

					for (i = 0; i < numactividades; i++) {
						if (actividades != null) {
							actividades[i] = $('#idciiu_' + i + '').val();
						}
					}

					console.warn(actividades);

					if ($scope.npn != null) {
						ConsultarDatosPredio.get({
							npn: $scope.npn,
							direccion: $scope.direccion,
							actividades: actividades,
							numactividades: numactividades
						}).$promise.then(
								function (response) {
									$(".overlap_espera").fadeOut(500, "linear");
									$(".overlap_espera_1").fadeOut(500, "linear");

									if (response.success) {
										$scope.messageErrorConsulta = "";
										$scope.Registroconsultausosuelo = response.data;
									} else {
										$scope.messageErrorConsulta = "No se encontró ningun registro asociado a la dirección " + $scope.direccion + ", por favor haga la búsqueda por el mapá. https://usodelsuelo.cali.gov.co/";
										//$scope.Registroconsultausosuelo = {direccion: $scope.Registroconsultausosuelo.direccion};
									}								
								},
								function (error) {
									$(".overlap_espera").fadeOut(500, "linear");
									$(".overlap_espera_1").fadeOut(500, "linear");
									$scope.Registroconsultausosuelo = {};
									$scope.messageErrorConsulta = "Ha ocurrido un error, por favor vuelva a intentarlo.";
									//console.error(error);
								}
						);
					} else {
						$(".overlap_espera").fadeOut(500, "linear");
						$(".overlap_espera_1").fadeOut(500, "linear");
						$scope.Registroconsultausosuelo = {};
						$scope.messageErrorConsulta = "Ha ocurrido un error, por favor vuelva a intentarlo.";
					}

				};
			}
		]);
