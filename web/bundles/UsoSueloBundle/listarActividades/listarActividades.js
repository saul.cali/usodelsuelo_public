saul.config(["$stateProvider",
	function ($stateProvider) {
		$stateProvider.state("listarActividadesUsoNpn", {
			url: "/usosuelo/listaractividades/{idComuna:int}/{idBarrio:int}/{idAreaActividad:int}/{idTipo:int}/{idVocacion:int}/{npn}/{direccion}",
			views: {
				"main": {
					controller: "listarActividadesCtrl",
					templateUrl: "bundles/UsoSueloBundle/listarActividades/listarActividades.tpl.html"
				}
			}
		})

				.state("listarActividadesNpn", {
					url: "/{idComuna:int}/{idBarrio:int}/{idAreaActividad:int}/{idTipo:int}/{idVocacion:int}/{npn}/{direccion}",
					views: {
						"main": {
							controller: "listarActividadesCtrl",
							templateUrl: "bundles/UsoSueloBundle/listarActividades/listarActividades.tpl.html"
						}
					}
				})

				.state("listarActividadesUso", {
					url: "/usosuelo/listaractividades/{idComuna:int}/{idBarrio:int}/{idAreaActividad:int}/{idTipo:int}/{idVocacion:int}",
					views: {
						"main": {
							controller: "listarActividadesCtrl",
							templateUrl: "bundles/UsoSueloBundle/listarActividades/listarActividades.tpl.html"
						}
					}
				})

				.state("listarActividades", {
					url: "/{idComuna:int}/{idBarrio:int}/{idAreaActividad:int}/{idTipo:int}/{idVocacion:int}",
					views: {
						"main": {
							controller: "listarActividadesCtrl",
							templateUrl: "bundles/UsoSueloBundle/listarActividades/listarActividades.tpl.html"
						}
					}
				});
	}


]).directive('tagManager', ['GuardarConsultaActividadesUsoSuelo', function (GuardarConsultaActividadesUsoSuelo) {
		return {
			restrict: 'E',
			scope: {
				tags: '=',
				autocomplete: '=autocomplete',
				request: '=',
				codigo: '=',
				fecha: '='
			},
			template:
					'<div class="tags">' +
					'<div ng-repeat="(idx, tag) in tags" class="tag"><span class="label label-success" ng-if = "tag.nombreConvencion === \'Permitido\'">{{ tag.codCiiu +" - "+tag.nombreCiiu +" - "+tag.nombreConvencion}} <a class="close" href ng-click="remove(idx)">×</a></span><span class="label label-danger" ng-if = "tag.nombreConvencion === \'No Permitido\'">{{ tag.codCiiu +" - "+tag.nombreCiiu +" - "+tag.nombreConvencion}} <a class="close" href ng-click="remove(idx)">×</a></span><span class="label label-warning" ng-if = "tag.nombreConvencion === \'Eir\'">{{ tag.codCiiu +" - "+tag.nombreCiiu +" - "+tag.nombreConvencion}} <a class="close" href ng-click="remove(idx)">×</a></span></div>' +
					'</div>' +
					'<div class=""><input type="text" id="buscar" class="form-control" placeholder="Por favor escriba la actividad que desea buscar" ng-model="newValue" /> ',
			// + '<span class="input-group-btn"><a class="btn btn-default" ng-click="add()" id="botonbuscar">Agregar</a></span></div>',
			link: function ($scope, $element) {

				$scope.seleccionados = [];

				var input = angular.element($element).find('input');
				// setup autocomplete
				if ($scope.autocomplete) {
					$scope.autocompleteFocus = function (event, ui) {
						input.val(ui.item.value);
						return false;
					};
					$scope.autocompleteSelect = function (event, ui) {
						$scope.newValue = ui.item.value;
						$scope.$apply($scope.add);
						return false;
					};
					$($element).find('input').autocomplete({
						minLength: 0,
						source: function (request, response) {
							var item;
							var itemMostrar;
							return response((function () {
								var _i, _len, _ref, _results;
								_ref = $scope.autocomplete;
								_results = [];
								for (_i = 0, _len = _ref.length; _i < _len; _i++) {

									//item = _ref[_i].codCiiu + " - " + _ref[_i].nombreCiiu + " - " + _ref[_i].palabraClave + " - " + _ref[_i].nombreConvencion + " - " + _ref[_i].respuesta;

									if (isNaN(request.term) === true) {
										item = _ref[_i].codCiiu + " - " + _ref[_i].nombreCiiu + " - " + _ref[_i].palabraClave + " - " + _ref[_i].nombreConvencion + " - " + _ref[_i].respuesta;
									} else {
										if (request.term.toLowerCase().length <= 4) {
											item = _ref[_i].codCiiu;
										}
									}

									var respuestaM = _ref[_i].respuesta.replace("<br>", " ");
									itemMostrar = _ref[_i].codCiiu + " - " + _ref[_i].nombreCiiu + " - " + _ref[_i].nombreConvencion + (respuestaM ? " - " + respuestaM : "");

									if (item.toLowerCase().indexOf(request.term.toLowerCase()) !== -1) {
										_results.push(itemMostrar);
									}
								}

								return _results;
							})());
						},
						focus: (function (_this) {
							return function (event, ui) {
								return $scope.autocompleteFocus(event, ui);
							};
						})(this),
						select: (function (_this) {
							return function (event, ui) {

								var str = ui.item.value;
								var codCiiu = str.split(" - ");
								//console.warn($scope.autocomplete);
								var indice = $scope.autocomplete.findIndex(function (element) {
									//console.warn(element.codCiiu==codCiiu[0]);
									return element.codCiiu == codCiiu[0];// && element.nombreCiiu == codCiiu[1] && element.nombreConvencion == codCiiu[2] && element.respuesta + "" == (codCiiu.length >= 4 ? codCiiu[3] : "");
								});
								//console.warn("Encontramos a: ", $scope.autocomplete[indice]);

								//$('#botonimprimirpantalla').show();
								//alert(codCiiu[0]);
								//console.log(ui.item.value);
								//$scope.save(ui.item.value);
								$('#botonbuscar').attr("disabled", false);
								$scope.request['codCiiu'] = $scope.autocomplete[indice].codCiiu;
								$scope.request['respuesta'] = $scope.autocomplete[indice].nombreConvencion;
								$scope.request['codigo'] = $scope.codigo;
								$scope.request['fecha'] = $scope.fecha;
								GuardarConsultaActividadesUsoSuelo.save($scope.request);
								return $scope.autocompleteSelect(event, ui);
							};
						})(this)
					}).data("ui-autocomplete")._renderItem = function (ul, item) {
						return $("<li></li>")
								.data("item.autocomplete", item)
								.append("<a>" + item.label + "</a>")
								.appendTo(ul);
					};
				}


				// adds the new tag to the array
				$scope.add = function () {
					var codCiiu = $scope.newValue.split(" - ");
					var indice = $scope.autocomplete.findIndex(function (element) {
						//console.warn(element.codCiiu==codCiiu[0]);
						if (element.respuesta == "null") {
							console.warn("es nulo");
						}
						return element.codCiiu == codCiiu[0];// && element.nombreCiiu == codCiiu[1] && element.nombreConvencion == codCiiu[2] && element.respuesta + "" == (codCiiu.length >= 4 ? codCiiu[3] : "");
					});
					//console.warn("Encontramos a: ", $scope.autocomplete[indice]);
					// if not dupe, add it
					if ($('#buscar').val() == "") {
						$('#botonbuscar').attr("disabled", true);
					}
					if ($('#buscar').val() != "") {
						//if ($scope.tags.indexOf($scope.newValue) == -1) {
						//console.warn("El nuevo valor es ...", $scope.newValue);

						if ($scope.tags.findIndex(function (element) {
							return element.codCiiu == $scope.autocomplete[indice].codCiiu;// && element.nombreCiiu == $scope.autocomplete[indice].nombreCiiu && element.nombreConvencion == $scope.autocomplete[indice].nombreConvencion && element.respuesta + "" == $scope.autocomplete[indice].respuesta;
						}) == -1) {
							$scope.tags.push($scope.autocomplete[indice]);

							var str = $scope.newValue;
							var respuesta = str.split("-");

						}

						/*if ($scope.tags.indexOf($scope.newValue) == -1) {
						 $scope.tags.push($scope.newValue);
						 
						 var str = $scope.newValue;
						 var respuesta = str.split("-");
						 
						 //alert(respuesta[2]);
						 }*/
						$scope.newValue = "";
					}
				};

				// remove an item
				$scope.remove = function (idx) {
					$scope.tags.splice(idx, 1);
					$('#botonbuscar').attr("disabled", true);
				};

				// capture keypresses
				input.bind('keypress', function (event) {
					// enter was pressed
					if (event.keyCode == 13) {
						$scope.$apply($scope.add);
					}
				});
			}
		};
	}])
		.controller('ModalCtrl', ['$scope', '$uibModalInstance', 'DataModal', function ($scope, $modalInstance, DataModal) {
				$scope.mensaje = DataModal.mensaje;
				$scope.titulo = DataModal.titulo;

				$scope.close = function () {
					$modalInstance.dismiss('cerrar');
					window.location.href = "https://usodelsuelo.cali.gov.co";
				};
			}])

		.controller("listarActividadesCtrl", ["$scope", "ListarActividadesUsoSuelo", '$stateParams', '$uibModal',
			function ($scope, ServicioListarActividadesUsoSuelo, $stateParams, $modal) {

				$scope.openModal = function (titulo, mensaje) {

					$scope.datosModal = {
						mensaje: mensaje,
						titulo: titulo
					};

					var modalInstance = $modal.open({
						templateUrl: "bundles/UsoSueloBundle/listarActividades/modal.tpl.html",
						controller: 'ModalCtrl',
						size: 'md',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							DataModal: function () {
								return $scope.datosModal;
							}
						}
					}).closed.then(function () {
						//alert('Callback ejecutado');
						//console.warn($scope.datosModal);
					});
				};



				$scope.init = function () {

					$scope.tags = [];
					$scope.allTags = [];
					$scope.npn = $stateParams.npn;
					$scope.direccion = $stateParams.direccion;

					if (angular.isUndefined($scope.npn) || $scope.npn == null || angular.isUndefined($scope.direccion) || $scope.direccion == null) {
						$scope.npn = 'SIN INFORMACION PREDIAL';
						$scope.direccion = 'SIN INFORMACION PREDIAL';
					}

					var request = {'currentPage': 1,
						'numPerPage': 1000, //aqui se carga la lista completa para hacer la busqueda
						'maxSize': 10,
						'orderBy': 'codCiiu',
						'idComuna': $stateParams.idComuna,
						'idBarrio': $stateParams.idBarrio,
						'idAreaActividad': $stateParams.idAreaActividad,
						'idTipo': $stateParams.idTipo,
						'idVocacion': $stateParams.idVocacion,
						'npn': $scope.npn,
						'direccion': $scope.direccion,
					};

					$scope.request = request;

					ServicioListarActividadesUsoSuelo
							.get(request)
							.$promise
							.then(function (response) {
								//console.log("entro al init");
								for (var i in response.alldata) {
									//$scope.allTags.push(response.data[i].codCiiu + ' - ' + response.data[i].nombreCiiu + ' - ' + response.data[i].nombreConvencion);
									if (response.alldata[i].respuesta !== "") {
										var respuesta = response.alldata[i].respuesta;
									} else {
										var respuesta = " ";
									}

									//$scope.allTags.push(response.data[i].codCiiu + ' - ' + response.data[i].nombreCiiu + ' - ' + response.data[i].nombreConvencion);
									$scope.allTags.push(response.alldata[i]);
								}
							});
				};

				$scope.tags = [];
				$scope.allTags = [];
				$scope.data = $stateParams;
				$scope.currentPage = 1;
				$scope.numPerPage = 20;
				$scope.maxSize = 10;
				$scope.rowWarning = 'estaEnviado';
				$scope.orderBy = 'codCiiu';
				$scope.headers = [
					{type: 'text', name: 'codCiiu', input: false, title: 'CodCiiu', header_align: 'text-center', body_align: 'text-center', width: '5%'},
					{type: 'text', name: 'nombreCiiu', input: false, title: 'Actividades Económicas CIIU', header_align: 'text-center', body_align: 'text-left', width: '33%'},
					{type: 'text', name: 'respuesta', input: false, title: 'Respuesta', header_align: 'text-center', body_align: 'text-left', width: '62%'}
				];

				$scope.actualizarDatos = function ()
				{
					var request = {'currentPage': $scope.currentPage,
						'numPerPage': $scope.numPerPage,
						'orderBy': $scope.orderBy,
						'idComuna': $stateParams.idComuna,
						'idBarrio': $stateParams.idBarrio,
						'idAreaActividad': $stateParams.idAreaActividad,
						'idTipo': $stateParams.idTipo,
						'idVocacion': $stateParams.idVocacion,
						'npn': $stateParams.npn,
						'direccion': $stateParams.direccion,
					};
					for (header in $scope.headers) {
						if (typeof $scope.headers[header].value !== 'undefined' && $scope.headers[header].value !== '') {
							//request.push({'name': $scope.headers[header].name, 'value': $scope.headers[header].value});
							request[$scope.headers[header].name] = $scope.headers[header].value;
						}
					}

					$(".overlap_espera").show();
					$(".overlap_espera_1").show();

					ServicioListarActividadesUsoSuelo
							.get(request)
							.$promise
							.then(function (response) {
								$(".overlap_espera").fadeOut(500, "linear");
								$(".overlap_espera_1").fadeOut(500, "linear");
								$scope.data = response['data'];

								if ($scope.data == "") {
									$scope.openModal("Buscar Actividades", "No se encontrarón actividades relacionadas al predio");
								} else if ($scope.data == null) {
									$scope.openModal("Error", "El Area, Tipo, Vocación No corresponden al Predio seleccionado, intente hacer la búsqueda de nuevo");
								} else if ($scope.data == 'nopermite') {
									$scope.openModal("ACTIVIDADES NO PERMITIDAS", "No se permite ningún tipo de actividad económica en el Predio seleccionado, intente hacer la búsqueda de nuevo");
								} else {
									$scope.totalItems = response['totalItems'];
									$scope.area = response['data'][0].areaActividad;
									$scope.tipo = response['data'][0].tipo;
									$scope.vocacion = response['data'][0].vocacion;
									$scope.comuna = response.comuna;
									$scope.barrio = response.barrio;
									$scope.corregimiento = response.corregimiento;
									$scope.vereda = response.vereda;

									if (response.direccion) {
										var direccion = response.direccion.replace(/_/g, " ");
										$scope.direccion = direccion;
										$scope.npn = response.npn;
									}
								}

								$scope.codigo = response.codigo;
								$scope.ip = response.ip;
								$scope.fecha = response.fecha;
								$scope.urls = response['url'];
							});
				};

				$scope.init();
				$scope.actualizarDatos();

				$('#botonbuscar').attr("disabled", true);

				$scope.ordenarPor = function (name)
				{
					$scope.orderBy = name;
					$scope.actualizarDatos();
				}


				$scope.imprimir = function () {
					$scope.impreso = true;
					window.open('usosuelo/imprimirpermitidos/' + $stateParams.idAreaActividad + '/' + $stateParams.idTipo + '/' + $stateParams.idVocacion + '/' + $scope.codigo + '/' + $scope.npn + '/' + $scope.direccion);
				};

				$scope.imprimirPantalla = function () {

					var encabezado = document.getElementById('encabezado').innerHTML;
					var listadoseleccionado = document.getElementById('listadoseleccionado').innerHTML;
					var codigo = document.getElementById('codigo').innerHTML;
					var footer = '<div style="font-family: Helvetica; font-size:8pt; text-align:justify; "><hr><br>El Concepto uso de suelo es un documento informativo y no autoriza el funcionamiento o apertura de establecimientos comerciales.<br>Para garantizar el adecuado desarrollo de la actividad económica se deberá adquirir la correspondiente licencia urbanística, que autorice la materialización del uso de suelo, por parte de los curadores urbanos.<br>La expedición de estos documentos informativos no otorga derechos ni obligaciones a su peticionario y no modifica los derechos conferidos mediante licencias urbanísticas que estén vigentes o que hayan sido ejecutadas.<br>El presente documento se expide con fundamento en lo establecido por el artículo 12 del Decreto 1203 de 2017, y las normas urbanísticas adoptadas por el Acuerdo 0373 de 2014.<br><br>Puede verificar su consulta posteriormente con el Código: <b>' + $scope.codigo + '</b>, accediendo al siguiente enlace. https://usodelsuelo.cali.gov.co/informacion#!/usosuelo/consultarcodigo/ </div>';

					var popupWin = window.open('', '_blank', 'width=800,height=600');
					popupWin.document.open();
					popupWin.document.write('<html><head></head><body onload="window.print()"><div style="font-family: Helvetica; font-size:13pt;">' + encabezado + '<div style="font-family: Helvetica; font-size:12pt;">' + listadoseleccionado + '</div><hr></div><h3>' + codigo + '</h3></body>' + footer + '</html>');
					popupWin.document.close();
				};

			}
		]);
