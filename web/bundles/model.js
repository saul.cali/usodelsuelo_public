saul.
		factory("ListarActividadesUsoSuelo", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + ":idComuna/:idBarrio/:idAreaActividad/:idTipo/:idVocacion/:npn/:direccion", {idComuna: 'idComuna', idBarrio: 'idBarrio', idAreaActividad: 'idAreaActividad', idTipo: 'idTipo', idVocacion: 'idVocacion', npn: 'npn', direccion: 'direccion'}, {
					'query': {method: 'GET', isArray: true, transformResponse: function (json, headerGetter) {
							return angular.fromJson(json);
						}},
					'get': {method: 'GET'},
				});
			}
		]).
		factory("ConsultarCodigoConsultaUsosuelo", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + "consultarcodigo/:codigo", {codigo: 'codigo'}, {
					'query': {method: 'GET', isArray: true, transformResponse: function (json, headerGetter) {
							return angular.fromJson(json);
						}},
					'get': {method: 'GET'},
				});
			}
		]).
				
		factory("ConsultaRespuestaUsosuelo", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + "consultarespuesta/:valor", {valor: 'valor'}, {
					'query': {method: 'GET', isArray: true, transformResponse: function (json, headerGetter) {
							return angular.fromJson(json);
						}},
					'get': {method: 'GET'}, 
				});
			}
		]).		
		
		factory("ConsultarDatosPredio", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + "usosuelo/consultarespuesta", {}, {
					'query': {method: 'GET', isArray: true, transformResponse: function (json, headerGetter) {
							return angular.fromJson(json);
						}},
					'get': {method: 'POST'},
				});
			}
		]).		
				
		factory("GuardarConsultaActividadesUsoSuelo", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + "usosuelo/guardarconsultaactividades", {}, {
					'save': {method: 'POST'}
				});
			}
		]).
				
		factory("ListarActividadesUsoxCiiu", [
			"$resource",
			"root",
			function ($resource, root) {
				return $resource(root + "usosuelo/consultarActividadesCiiu", {
					'query': {method: 'GET', isArray: true, transformResponse: function (json, headerGetter) {
							return angular.fromJson(json);
						}},
					'get': {method: 'GET'},
				});
			}
		]).
							
//Servicio para abrir un formulario en otra ventana. Es útil en caso de previsualización de un pdf
		factory("UtilForm", [function () {
				return {
					/**
					 * Abre una nueva ventana con los parámetros especificados
					 * @param  {String}     url    Url que se va a abrir
					 * @param  {String}     method Puede ser post o get
					 * @param  {Array}      data   Arreglo con objetos {name, value} que serán los datos enviados
					 */
					openWith: function (url, method, data) {
						var f = document.createElement('form');
						f.innerHTML = '';
						f.setAttribute('method', method);
						f.setAttribute('action', url);
						f.setAttribute('style', 'display: none');

						for (var i = data.length - 1; i >= 0; i--) {
							var input = document.createElement("input");
							input.setAttribute('type', "text");
							input.setAttribute('name', data[i].name);
							input.setAttribute('value', data[i].value);
							f.appendChild(input);
						}

						document.body.appendChild(f);

						var win = window.open('', 'ventanaPrevisualizacion');
						f.target = 'ventanaPrevisualizacion';
						f.submit();

						f.parentElement.removeChild(f);
					}
				};
			}])
