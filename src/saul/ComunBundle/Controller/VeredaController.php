<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @Route("/vereda")
*/
class VeredaController extends Controller 
{

    
    /**  
     * @Route("/")
     * @Method({"GET"})
     */
    public function consultarAction() 
    {
        $serializer = $this->get('serializer');
        $arrayVereda = $this->getDoctrine()->getRepository('ComunBundle:Vereda')->findAll();
        return new Response($serializer->serialize($arrayVereda, 'json'));
    }
    
    

}
