<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @Route("/corregimiento")
*/
class CorregimientoController extends Controller 
{

    
    /**  
     * @Route("/")
     * @Method({"GET"})
     */
    public function consultarAction() 
    {
        $serializer = $this->get('serializer');
        $array = $this->getDoctrine()->getRepository('ComunBundle:Corregimiento')->findAll();
        return new Response($serializer->serialize($array, 'json'));
    }
    
    

}
