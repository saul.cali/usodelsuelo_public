<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Emarref\Jwt\Claim;
use Emarref\Jwt\Token;
use Emarref\Jwt\Jwt;
use Emarref\Jwt\Algorithm\Hs256;
use Emarref\Jwt\Encryption\Factory;
use Emarref\Jwt\Verification\Context;
use saul\ComunBundle\Classes\Parametro;

class SeguridadController extends Controller
{     
    /**
     * Funci�n que retorna el token de acuerdo a los paramtros del sistema.
     * Los parametros son: audiencia, usuario y contrase�a.
     * Esta funci�n utiliza el Bundle de Jwt. https://github.com/emarref/jwt
     * 
     * @return string Token serializado.
     */
    public function obtenerToken($container)
    {
        $token = new Token();
        $token->addClaim(new Claim\Audience([Parametro::obtener($container, 'audiencia')]));
        $token->addClaim(new Claim\Expiration(new \DateTime('30 minutes')));
        $token->addClaim(new Claim\IssuedAt(new \DateTime('now')));
        $token->addClaim(new Claim\JwtId(Parametro::obtener($container, 'usuario')));
        $notBefore = new \DateTime('now');
        $time = 5;
        $notBefore->sub(new \DateInterval('PT'.$time.'M'));
        $token->addClaim(new Claim\NotBefore());
        $jwt = new Jwt();
        $contrasena = Parametro::obtener($container, 'contrasena');
        $algorithm = new Hs256($contrasena);
        $encryption = Factory::create($algorithm);
        $serializedToken = $jwt->serialize($token, $encryption);
        return $serializedToken;
    }
    
     /*
     * Funci�n que se encarga de recibir un usuario y con este validar la seguridad
     * mediante un Json Web Token.
     *      
     * @param saul\IntegracionBundle\UsuarioIntegracion $objUsuario
     * @param string $serializedToken
     * @return array
     */
    public function validarUsuario($objUsuario, $serializedToken) {
        try {            
            $jwt = new Jwt();
            $token = $jwt->deserialize($serializedToken);
            $algorithm = new Hs256($objUsuario->getContrasena());
            $encryption = Factory::create($algorithm);
            $context = new Context($encryption);
            $context->setAudience($objUsuario->getHalt());
            $jwt->verify($token, $context);			
            return array('success' => true);
        } catch (Emarref\Jwt\VerificacionException $e) {
            return array('success' => 0);
        }
    }
    
    /**
     * Funci�n que retorna el token de acuerdo a los paramtros del sistema.
     * Los parametros son: audiencia, usuario y contrase�a, este se realiza con 
     * la informaci�n de un usuario.
     * Esta funci�n utiliza el Bundle de Jwt. https://github.com/emarref/jwt
     * 
     * @return string Token serializado.
     */
    public function obtenerTokenDeUsuario($container, $objUsuario)
    {
        $token = new Token();
        $token->addClaim(new Claim\Audience([$objUsuario->getHalt()]));
        $token->addClaim(new Claim\Expiration(new \DateTime('30 minutes')));
        $token->addClaim(new Claim\IssuedAt(new \DateTime('now')));
        $token->addClaim(new Claim\JwtId($objUsuario->getNombre()));
        $notBefore = new \DateTime('now');
        $time = 5;
        $notBefore->sub(new \DateInterval('PT'.$time.'M'));
        $token->addClaim(new Claim\NotBefore());
        $jwt = new Jwt();
        $contrasena = $objUsuario->getContrasena();
        $algorithm = new Hs256($contrasena);
        $encryption = Factory::create($algorithm);
        $serializedToken = $jwt->serialize($token, $encryption);
        return $serializedToken;
    }
}
