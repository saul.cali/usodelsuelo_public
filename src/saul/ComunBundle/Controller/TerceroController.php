<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use saul\ComunBundle\Entity\Tercero;


/**
 * SolicitudController
 *
 * Controla todo lo relacionado con una solicitud, creación, actualización y elminación.
 * Su acceso es mediante peticiones http RestFul.
 *
 * @package    ComunBundle
 * @author     José Julián Barbosa Ayala <julian.barbosa@gmail.com>
 * @version    0.1
 */

class TerceroController extends Controller {

    /**
     * Función que almacena los datos de un Tercero.
     *
     * @param EntityManager $em
     * @param Solicitud $objSolicitud
     * @param Array $data
     * @param String $errorString
     * @access private
     */
    public function almacenarTercero(&$em, $data, $objRepresentante=null)
    {        
		try {		
			$arrayTercero = $em->getRepository('ComunBundle:Tercero')->findOneBy(['identificacion'=>$data['identificacion']]);        
		} catch(\Exception $e) {
			print_r($data);
			die();
		}
        if(isset($data['id'])) {
            $objTercero = $em->getRepository('ComunBundle:Tercero')->find($data['id']);
        } else {
            if(!$arrayTercero) {
				$objTercero = new Tercero();
                //return ['success'=>false, 'msg'=> 'Error el tercero '.$data['identificacion'].' ya existe.', 'result'=>$arrayTercero[0] ];
			} else {
				$objTercero = $arrayTercero;
			}             
        }        

        $objTercero->setIdentificacion($data['identificacion']);
		if(isset($data['tipoIdentificacion']['idTipoDocIdentificacion'])) {
			$objTercero->setTipoIdentificacion($em->getReference('saul\ComunBundle\Entity\TipoDocIdentificacion',$data['tipoIdentificacion']['idTipoDocIdentificacion']));
		} else {			
			$objTipoIdentificacion = $em->getRepository('ComunBundle:TipoDocIdentificacion')->findOneBy(['descripcion'=>trim($data['tipoIdentificacion']['descripcion'])]);			
			$objTercero->setTipoIdentificacion($em->getReference('saul\ComunBundle\Entity\TipoDocIdentificacion',$objTipoIdentificacion->getIdTipoDocIdentificacion()));
		}        
        $objTercero->setNombre(isset($data['nombre'])?$data['nombre']:'');
        $objTercero->setApellido(isset($data['apellido'])?$data['apellido']:'');
        $objTercero->setRazonSocial(isset($data['razonSocial'])?$data['razonSocial']:'');
		$objTercero->setDireccion(isset($data['direccion'])?$data['direccion']:'');
		$objTercero->setInformacionAdicional(isset($data['informacionAdicional'])?$data['informacionAdicional']:'');
		$objTercero->setTelefono(isset($data['telefono'])?$data['telefono']:'');		
		$objTercero->setEmail(isset($data['email'])?$data['email']:'');
		if($objRepresentante!=null) {
			$objTercero->setRepresentante($em->getReference('saul\ComunBundle\Entity\Tercero',$objRepresentante->getId()));
		}
		if(isset($data['ciudadNacimiento']['idCiudad']) && $data['ciudadNacimiento']['idCiudad']!==null) {
			$objTercero->setCiudadNacimiento($em->getReference('saul\ComunBundle\Entity\Ciudad',$data['ciudadNacimiento']['idCiudad']));
		}
		if(isset($data['ciudadResidencia']['idCiudad']) && $data['ciudadResidencia']['idCiudad']!=null) {
			$objTercero->setCiudadResidencia($em->getReference('saul\ComunBundle\Entity\Ciudad',$data['ciudadResidencia']['idCiudad']));
		}
        $em->persist($objTercero);
        $em->flush();
        return ['success'=>true, 'msg'=>'Exito al almacenar.', 'result'=>$objTercero];
    }

	/**  
     * @Route("tercero/{clave}")
     * @Method({"GET"})
     */
    public function consultarPorClaveAction($clave) 
    {
        $serializer = $this->get('serializer');
        $arrayTercero = $this->getDoctrine()->getRepository('ComunBundle:Tercero')->consultarPorClave($clave);
        return new Response($serializer->serialize($arrayTercero, 'json'));
    }  

}