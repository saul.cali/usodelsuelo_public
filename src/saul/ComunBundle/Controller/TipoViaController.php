<?php

namespace saul\ComunBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class TipoViaController extends Controller
{
    /**
     * @Route("/tipo_via")
     * @Method({"GET"})
     */
    public function consultaAction()
    {
        $em = $this->get('doctrine')->getManager();
        $result = $em->getRepository('ComunBundle:PnItem')->consultar(1);
        return new Response($this->get('serializer')->serialize($result, 'json'));
    }
}
