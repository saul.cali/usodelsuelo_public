<?php

namespace saul\ComunBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return new Response(json_encode(['msg'=>'Dirijase a https://planeacion.cali.gov.co/saul'])); 
    }
	
	/**
     * @Route("/accesodenegado")
     */
    public function accesoDenegadoAction()
    {
        return new Response(json_encode(['msg'=>'Usted no tiene permisos para acceder a este servicio'])); 
    }
}
