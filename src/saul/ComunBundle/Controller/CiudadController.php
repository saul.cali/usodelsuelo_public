<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @Route("/ciudad")
*/
class CiudadController extends Controller 
{
    
    /**  
     * @Route("/{clave}")
     * @Method({"GET"})
     */
    public function consultarPorClaveAction($clave) 
    {
        $serializer = $this->get('serializer');
        $arrayComuna = $this->getDoctrine()->getRepository('ComunBundle:Ciudad')->consultarPorClave($clave);
        return new Response($serializer->serialize($arrayComuna, 'json'));
    }  

}

