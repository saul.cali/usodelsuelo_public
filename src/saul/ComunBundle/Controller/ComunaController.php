<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @Route("/comuna")
*/
class ComunaController extends Controller 
{

    /**  
     * @Route("/")
     * @Method({"GET"})
     */
    public function consultar2Action() 
    {
        $serializer = $this->get('serializer');
        $arrayComuna = $this->getDoctrine()->getRepository('ComunBundle:Comuna')->consultar();
        return new Response($serializer->serialize($arrayComuna, 'json'));
    }
    
    /**  
     * @Route("/all")
     * @Method({"GET"})
     */
    public function consultarAction() 
    {
        $serializer = $this->get('serializer');
        $arrayComuna = $this->getDoctrine()->getRepository('ComunBundle:Comuna')->findAll();
        return new Response($serializer->serialize($arrayComuna, 'json'));
    }
    
    

}
