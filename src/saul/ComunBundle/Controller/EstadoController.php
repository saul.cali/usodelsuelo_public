<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class EstadoController extends Controller 
{   
    /**  
     * @Route("/estado/consultar_por_solicitud")
     * @Method({"GET"})
     */
    public function consultarEstadoAction(Request $request) {
        
        $serializer = $this->get('serializer');
        $objSolicitud = $this->getDoctrine()->getRepository('SolicitudBundle:Solicitud')->find($request->query->get('idsolicitud'));
        //die("tiposolicitud=>".$objSolicitud->getTipoSolicitud()->getIdTipoSolicitud());
        $arrayEstados = $this->getDoctrine()->getRepository('ComunBundle:Estado')->consultarPorSolicitud($objSolicitud->getTipoSolicitud()->getIdTipoSolicitud());                
        return new Response($serializer->serialize($arrayEstados, 'json'));
    }   

}

