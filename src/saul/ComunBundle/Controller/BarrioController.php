<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @Route("/barrio")
*/
class BarrioController extends Controller 
{
    /**  
     * @Route("/")
     * @Method({"GET"})
     */
    public function consultar2Action(Request $request) 
    {
        $serializer = $this->get('serializer');
        $arrayComuna = $this->getDoctrine()->getRepository('ComunBundle:Barrio')->consultar($request->query->get('idcomuna'));
        return new Response($serializer->serialize($arrayComuna, 'json'));
    }  
    
    /**  
     * @Route("/{clave}")
     * @Method({"GET"})
     */
    public function consultarPorClaveAction($clave) 
    {
        $serializer = $this->get('serializer');
        $arrayComuna = $this->getDoctrine()->getRepository('ComunBundle:Barrio')->consultarPorClave($clave);
        return new Response($serializer->serialize($arrayComuna, 'json'));
    }  

}

