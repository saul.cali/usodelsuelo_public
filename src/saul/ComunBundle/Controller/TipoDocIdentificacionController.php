<?php

namespace saul\ComunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use saul\ComunBundle\Entity\TipoDocIdentificacion;

/**
* @Route("/tipodocidentificacion")
*/
class TipoDocIdentificacionController extends Controller
{
    /**
     * @Route("/all")
     */
    public function allAction()
    {
    	$tipos = $this->getDoctrine()->getRepository(TipoDocIdentificacion::class)->findAll();
    	return new Response(
    		$this->get('serializer')->serialize($tipos, 'json')
    	);
    }

}
