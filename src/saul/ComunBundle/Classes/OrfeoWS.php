<?php

namespace saul\ComunBundle\Classes;

use saul\ComunBundle\Classes\Parametro;

/**
 * OrfeoWS
 *
 *
 * Radicar en Orfeo consumiendo el servicio web(webservices) por dependencia
 *
 *
 * @package    ruva
 * @author     Alejandro Ordoñez Del Valle
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class OrfeoWS {

    const
	    ENTRADA = "2",
	    SALIDA = "1",
	    INFORMATIVO = "4";

    protected
	    $infoRemitente = array(),
	    $remitente2 = null,
	    $remitente3 = null,
	    $parametrosWS = array(),
	    $errorOrfeo = null,
	    $urlConsumo = null,
	    $emailSOU = null,
	    $clienteWS = null,
	    $otro = "Solicitud",
	    $tipoRadicado = self::ENTRADA,
	    $dirDocumentos = null;
    public
	    $docIdentidadSOU = null, //Documento de Identidad de quien radica, Documento de Identidad del Subdirector del SOU
	    $observacion = 'SAUL',
	    $numRadicadoPadre = null,
	    $dependencia = null,
	    $dependenciaDestino = null,
	    $subdependencia = null,
	    $numSolicitud = null,
	    $numRadicado = null,
	    $docIdentidadRemitente = null,
	    $nombreRemitente = null,
	    $apellidoRemitente = null,
	    $telefonoRemitente = null,
	    $direccionRemitente = null,
	    $emailRemitente = null,
	    $archivo = null,
	    $extensionArchivo = 'pdf',
	    $serie = null,
	    $subserie = null,
	    $tipoDocumental = null,
	    $asunto = null,
	    $tipoEmp = null,
	    $idCont = null,
	    $codPais = null,
	    $codDpto = null,
	    $codMunicipio = null,
	    $logger = null;

    function __construct($container) {
	$this->container = $container;
    }

    /**
     * Crea un radicado en Orfeo
     */
    public function crearRadicado() {
	$arrayParametrosEnvio['idsolicitud'] = $this->numSolicitud;
	$arrayParametrosEnvio['usuario_numidentificacion'] = $this->docIdentidadRemitente;
	$arrayParametrosEnvio['usuario_first_name'] = $this->nombreRemitente;
	$arrayParametrosEnvio['usuario_last_name'] = $this->apellidoRemitente;
	$arrayParametrosEnvio['usuario_email_address'] = $this->emailRemitente;
	$arrayParametrosEnvio['usuario_direccion'] = $this->direccionRemitente;
	$arrayParametrosEnvio['usuario_telefono'] = $this->telefonoRemitente;
	$arrayParametrosEnvio['solicitud_tipodocumental'] = $this->tipoDocumental; //1529;
	$arrayParametrosEnvio['solicitud_seriedocumental'] = $this->serie; //5;
	$arrayParametrosEnvio['solicitud_subseriedocumental'] = $this->subserie; //2;
	$arrayParametrosEnvio['solicitud_subdependencia'] = $this->subdependencia; //80;
	$arrayParametrosEnvio['solicitud_dependencia'] = $this->dependencia;
	$arrayParametrosEnvio['id_radicador_orfeo'] = $this->docIdentidadSOU;
	$arrayParametrosEnvio['otro'] = $this->otro;
	$arrayParametrosEnvio['radicado_padre'] = $this->numRadicadoPadre;
	$arrayParametrosEnvio['tipoRadicado'] = $this->tipoRadicado;
	$arrayParametrosEnvio['nombreTipoSolicitud'] = $this->nombreTipoSolicitud;
	$arrayParametrosEnvio['dependencia_destino'] = $this->dependenciaDestino;

	$url = Parametro::obtener($this->container, 'url_app') . '/solicitud/radicar';
	$defaults = array(
	    CURLOPT_URL => $url,
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $arrayParametrosEnvio,
	    CURLOPT_RETURNTRANSFER => true, // return web page
	    CURLOPT_VERBOSE => 1, //
	    CURLOPT_HEADER => false, //don't return headers
	    CURLOPT_FOLLOWLOCATION => true, // follow redirects
	    CURLOPT_CONNECTTIMEOUT => 120, //time-out on connect
	    CURLOPT_TIMEOUT => 0, // time-out on response,
	    CURLOPT_FOLLOWLOCATION => 1,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_SSL_VERIFYHOST => false
	);
	$ch = curl_init();
	curl_setopt_array($ch, $defaults);
	$result = curl_exec($ch);

	$codigoRespuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$estaEnviado = true;
	$numRadicado = null;
	if ($codigoRespuesta == '200') {
	    $arrayResult = json_decode($result);
	    if (isset($arrayResult->success) && $arrayResult->success == true) {
		$numRadicado = $arrayResult->numRadicado;
		$this->numRadicado = $numRadicado;
		return array("success" => true, "numRadicado" => $numRadicado);
	    } else {
		return $result;
	    }
	} else {
	    return array("success" => false, "result" => print_r($result));
	}
    }

    /**
     * Devuelve el numero del radicado de Orfeo
     * @return string
     */
    public function obtenerRadicado() {
	return $this->numRadicado;
    }

    /**
     * Establece el numero de la Solicitud
     * @param integer $numSolicitud
     */
    public function estableceNumSolicitud($numSolicitud) {
	$this->numSolicitud = $numSolicitud;
    }

    /**
     * Carga un documento tipo plantilla al radicado de Orfeo
     */
    public function cargarPlantilla() {
	$arrayParametrosEnvio['idsolicitud'] = $this->numSolicitud;
	$arrayParametrosEnvio['usuario_numidentificacion'] = $this->docIdentidadRemitente;
	$arrayParametrosEnvio['usuario_first_name'] = $this->nombreRemitente;
	$arrayParametrosEnvio['usuario_last_name'] = $this->apellidoRemitente;
	$arrayParametrosEnvio['usuario_email_address'] = $this->emailRemitente;
	$arrayParametrosEnvio['usuario_direccion'] = $this->direccionRemitente;
	$arrayParametrosEnvio['usuario_telefono'] = $this->telefonoRemitente;
	$arrayParametrosEnvio['solicitud_tipodocumental'] = $this->tipoDocumental; //1529;
	$arrayParametrosEnvio['solicitud_seriedocumental'] = $this->serie; //5
	$arrayParametrosEnvio['solicitud_subseriedocumental'] = $this->subserie; //2
	$arrayParametrosEnvio['solicitud_subdependencia'] = $this->subdependencia; //80
	$arrayParametrosEnvio['solicitud_dependencia'] = $this->dependencia;
	$arrayParametrosEnvio['id_radicador_orfeo'] = $this->docIdentidadSOU;
	$archivoB64 = base64_encode(file_get_contents(WEB_DIRECTORY . DIRECTORY_SEPARATOR . $this->archivo));
	$arrayParametrosEnvio['archivoB64'] = $archivoB64;
	$arrayParametrosEnvio['numRadicado'] = $this->numRadicado;
	$arrayParametrosEnvio['tipoRadicado'] = $this->tipoRadicado;
	$arrayParametrosEnvio['radicado_padre'] = $this->numRadicadoPadre;
	$defaults = array(
	    CURLOPT_URL => Parametro::obtener($this->container, 'url_app') . '/solicitud/cargar',
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $arrayParametrosEnvio,
	    CURLOPT_RETURNTRANSFER => true, // return web page
	    CURLOPT_VERBOSE => 1, //
	    CURLOPT_HEADER => false, //don't return headers
	    CURLOPT_FOLLOWLOCATION => true, // follow redirects
	    CURLOPT_CONNECTTIMEOUT => 120, //time-out on connect
	    CURLOPT_TIMEOUT => 0, // time-out on response,
	    CURLOPT_FOLLOWLOCATION => 1,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_SSL_VERIFYHOST => false
	);
	$ch = curl_init();
	curl_setopt_array($ch, $defaults);
	$result = curl_exec($ch);
	$codigoRespuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$estaEnviado = true;
	$msg = null;
	if ($codigoRespuesta == '200') {
	    $arrayResult = json_decode($result);
	    if (isset($arrayResult->success)) {
		$msg = $arrayResult->msg;
	    }
	} else {
	    $estaEnviado = false;
	}
	return array("success" => $estaEnviado, "msg" => $msg);
    }

    /**
     * Adiciona documentos al Orfeo radicado
     * @param array $documentos arreglo de documentosxsolicitud
     *
     * El array debe tener la siguiente estructura
     * array (0 => array('archivo'    => 'nombre archivo.extencion archivo',
     *                   'Documento'  => array( 'nombre'      => 'Nombre del archivo',
     *                                          'descripcion' => 'Descripcion del archivo')));
     */
    public function adjuntarDocumentos(array $documento) {
	$this->docIdentidadSOU = Parametro::obtener($this->container, 'idradicador_ws_orfeo');
	$arrayParametrosEnvio = array();
	$arrayParametrosEnvio['numRadicado'] = $this->numRadicado;
	$arrayParametrosEnvio['docIdentidadSOU'] = $this->docIdentidadSOU;
	$explode = explode('.', $documento["archivo"]);
	$this->extensionArchivo = $explode[1];
	$arrayParametrosEnvio["archivo"] = base64_encode(file_get_contents(WEB_DIRECTORY . DIRECTORY_SEPARATOR . $documento["archivo"]));
	$arrayParametrosEnvio["nombre"] = htmlentities($documento["nombre"], ENT_QUOTES, 'UTF-8') . '.' . $this->extensionArchivo;
	$arrayParametrosEnvio["descripcion"] = htmlentities($documento["nombre"], ENT_QUOTES, 'UTF-8');
	$defaults = array(
	    CURLOPT_URL => Parametro::obtener($this->container, 'url_app') . '/solicitud/adjuntar',
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $arrayParametrosEnvio,
	    CURLOPT_RETURNTRANSFER => true, // return web page
	    CURLOPT_VERBOSE => 1, //
	    CURLOPT_HEADER => false, //don't return headers
	    CURLOPT_FOLLOWLOCATION => true, // follow redirects
	    CURLOPT_CONNECTTIMEOUT => 120, //time-out on connect
	    CURLOPT_TIMEOUT => 0, // time-out on response,
	    CURLOPT_FOLLOWLOCATION => 1,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_SSL_VERIFYHOST => false
	);
	$ch = curl_init();
	curl_setopt_array($ch, $defaults);
	$result = curl_exec($ch);
	$codigoRespuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$estaEnviado = true;
	$msg = null;
	if ($codigoRespuesta == '200') {
	    $arrayResult = json_decode($result);
	    if (isset($arrayResult->success)) {
		$msg = $arrayResult->msg;
	    }
	} else {
	    $estaEnviado = false;
	}

	return array("success" => $estaEnviado, "msg" => $msg);
    }

    /**
     * archiva el radicado de Orfeo
     * @return string
     */
    public function cerrarRadicado() {
	$arrayParametrosEnvio['numRadicado'] = $this->numRadicado;
	$defaults = array(
	    CURLOPT_URL => Parametro::obtener($this->container, 'url_app') . '/solicitud/cerrar',
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $arrayParametrosEnvio,
	    CURLOPT_RETURNTRANSFER => true, // return web page
	    CURLOPT_VERBOSE => 1, //
	    CURLOPT_HEADER => false, //don't return headers
	    CURLOPT_FOLLOWLOCATION => true, // follow redirects
	    CURLOPT_CONNECTTIMEOUT => 120, //time-out on connect
	    CURLOPT_TIMEOUT => 0, // time-out on response,
	    CURLOPT_FOLLOWLOCATION => 1,
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_SSL_VERIFYHOST => false
	);
	$ch = curl_init();
	curl_setopt_array($ch, $defaults);
	$result = curl_exec($ch);
	$codigoRespuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$estaEnviado = true;
	$numRadicado = null;
	if ($codigoRespuesta == '200') {
	    $arrayResult = json_decode($result);
	    if (isset($arrayResult->success)) {
		$numRadicado = $arrayResult->numRadicado;
		$this->numRadicado = $numRadicado;
	    }
	} else {
	    $estaEnviado = false;
	}
	return array("success" => $estaEnviado, "numRadicado" => $numRadicado);
    }

    public function radicarOrfeo($solicitud, $usuario) {
	$this->estableceNumSolicitud($solicitud->getIdsolicitud());
	$this->docIdentidadRemitente = $usuario->getNumidentificacion();
	$this->nombreRemitente = $usuario->getFirstName();
	$this->apellidoRemitente = $usuario->getLastName();
	$this->emailRemitente = $usuario->getEmailAddress();
	$this->direccionRemitente = $usuario->getDireccion();
	$this->telefonoRemitente = $usuario->getTelefono();
	$this->tipoDocumental = $solicitud->getTiposolicitud()->getTipodocumental();
	$this->serie = $solicitud->getTiposolicitud()->getSeriedocumental();
	$this->subserie = $solicitud->getTiposolicitud()->getSubseriedocumental();
	$this->subdependencia = $solicitud->getTiposolicitud()->getSubdependencia();
	$this->otro = $solicitud->getTiposolicitud()->getNombreOrfeo();
	$this->nombreTipoSolicitud = $solicitud->getTiposolicitud()->getNombre();
	$this->dependencia = $solicitud->getTiposolicitud()->getSubdireccion()->getDependenciaorfeo();
	$this->docIdentidadSOU = $solicitud->getTiposolicitud()->getSubdireccion()->getIdRadicadorOrfeo();
	$this->dependenciaDestino = $solicitud->getTiposolicitud()->getSubdireccion()->getIdRadicadorOrfeo();
	$this->establecerParametrosWS();
	$result = $this->crearRadicado();
	return $result;
    }

    /**
     * Establece un valor al numero de radicadoPadre
     * @param string $numRadicado numero del radicado
     */
    public function establecerRadicadoPadre($numRadicado) {
	$this->numRadicadoPadre = $numRadicado;
    }

    /**
     * Establece el tipo de radicado a realizar
     * @param string $tipoRadicado
     */
    public function establecerTipoRadicado($tipoRadicado) {
	$this->tipoRadicado = $tipoRadicado;
	if ($tipoRadicado == self::SALIDA)
	    $this->otro = 'Respuesta';
	else if ($tipoRadicado == self::INFORMATIVO)
	    $this->otro = 'Informativo';
	else
	    $this->otro = 'Solicitud';
    }

    /**
     * Establece un valor al numero de radicado
     * @param string $numRadicado
     */
    public function establecerRadicado($numRadicado) {
	$this->numRadicado = $numRadicado;
    }

    public function getTipoRadicado() {
	return $this->tipoRadicado;
    }

    /**
     * Establace los valores de los parametrosWS
     */
    public function establecerParametrosWS() {
	$TRD = $this->serie . '.' . $this->subserie . '.' . $this->tipoDocumental;
	//$TRD = '10.01.10'; // prueba

	/*        if ($this->tipoRadicado == self::SALIDA)
	  {
	  $this->tipoEmp = null;
	  $this->idCont = null;
	  $this->codPais = null;
	  $this->codDpto = null;
	  $this->codMunicipio = null;
	  }
	 *
	 */
	$this->infoRemitente = array(
	    'documento' => '0', //Tipo de Documento, 0 es cedula
	    'cc_documento' => $this->docIdentidadRemitente, //Numero Documento del usuario a ingresar.51923592 sonia
	    'tipo_emp' => $this->tipoEmp, //Si es Ciudadano 1, OEM 2, Entidad 3
	    'nombre' => htmlentities($this->nombreRemitente, ENT_QUOTES, 'UTF-8'), //Nombres del Remitente / destinatario seg�n sea el caso
	    'prim_apel' => htmlentities($this->apellidoRemitente, ENT_QUOTES, 'UTF-8'), //Apellido
	    'seg_apel' => '', //Seg Apellido
	    'telefono' => $this->telefonoRemitente, //Telefono   del
	    'direccion' => htmlentities($this->direccionRemitente, ENT_QUOTES, 'UTF-8'),
	    'mail' => $this->emailRemitente,
	    'otro' => $this->otro,
	    'idcont' => $this->idCont,
	    'idpais' => $this->codPais, // Codigo del Pais donde se encuentra el remitente y/o Destino
	    'codep' => $this->codDpto, // Codigo del Departamento
	    'muni' => $this->codMunicipio       // Codigo del municipio
	);
	$this->parametrosWS = array(
	    $this->emailSOU,
	    $this->infoRemitente,
	    htmlentities($this->remitente2, ENT_QUOTES, 'UTF-8'),
	    htmlentities($this->remitente3, ENT_QUOTES, 'UTF-8'),
	    $this->asunto, //Radicacion Solicitud desde afuera numero interno, En esta posicion debe ir el asunto
	    "3", // Medio de recepcion /envio.  Este codigoes seg�n la tabla existente en orfeo �MEDIO_RECEPCION� (1-correo, 2-fax, 3-Internet, 4-mail... )
	    "7", // Descripcion Anexos
	    $this->dependencia . $this->subdependencia, // Codigo Dependencia Radicadora.  De este codigo extraera la secuencia del radicado.
	    $this->tipoRadicado, // Tipo de Radicado 2 Entrada, 1 Salida.
	    "4", // Cuenta Interna/Oficion o algun codigo identificador del documento.
	    $this->dependencia . $this->subdependencia, // Dependencia Destino
	    "2", // Tipo de remitente.  Asociado con la tabla tipo_remitente (en la tabla de uds 0-Entidades,1-Otras empresas,2-Persona natural,3-Predio,5-,O-Oros,6-Funcionario)
	    $TRD, // TRD de la forma (serie.subserie.tipoDocumental), //Antes se utilizaba para Tipo Remitente
	    "0", // Tipo de Documento. Asociado a la tabla tipo_doc_identificacion con datos (0-Cedula de Ciudadania, 1-Tarjeta de Identidad,2-Cedula de Extranjer�a,3-Pasaporte, 4-Nit  ,5-NUIR)
	    "0", // Codigo de carpeta a la cual se quiere enviar el radicado.  0 � La bandeja de entrada. (Depende tambien de si es personal o general)
	    "0", // Codigo tipo de carpeta.  0- Carpetas generales (Entrada, salida, internas)   1- Codigo de carpetas personales.
	    $this->docIdentidadSOU // Documento de identificacion de la persona radicadora.  Esta debe existir en OrfeoGPL.
	);
	if ($this->tipoRadicado == self::SALIDA) {
	    $this->parametrosWS[17] = $this->numRadicadoPadre;
	}
	//print_r($this->parametrosWS);
    }

    /**
     * establece el directorio de documentos
     */
    public function establecerDirDocumentos($dirDocumentos) {
	$this->dirDocumentos = $dirDocumentos;
    }

    /**
     * Obtener el consecutivo interno que lleva un número de radicado, necesario para formar la TRD del documento
     * @param string $numRadicado
     * @return string
     */
    static function obtenerConsecutivoOrfeo($numRadicado) {
	return substr($numRadicado, strlen($numRadicado) - 7, 6);
    }

    public function validarDatosUsuario(sfGuardUser $usuario) {
	if (is_numeric($usuario->username)) {
	    return TRUE;
	} else {
	    return FALSE;
	}
    }

    public function setRemitente($docIdentidadRemitente, $tipoEmp, $nombreRemitente, $apellidoRemitente, $telefonoRemitente, $direccionRemitente, $emailRemitente) {
	$this->docIdentidadRemitente = $docIdentidadRemitente;
	$this->tipoEmp = $tipoEmp;
	$this->nombreRemitente = $nombreRemitente;
	$this->apellidoRemitente = $apellidoRemitente;
	$this->telefonoRemitente = $telefonoRemitente;
	$this->direccionRemitente = $direccionRemitente;
	$this->emailRemitente = $emailRemitente;
    }

    public function setTipoDocumental($tipoDocumental, $serie, $subserie, $subdependencia) {
	$this->tipoDocumental = $tipoDocumental;
	$this->serie = $serie;
	$this->subserie = $subserie;
	$this->subdependencia = $subdependencia;
    }

    /**
     * Establece el código xxxxxx
     * @param type $otro
     */
    public function establecerOtro($otro) {
	$this->otro = $otro;
    }

    private function radicarOrfeoConFirmaDigital($objSolicitud, $objSolicitante, $dataPdf, $classTemplate, $tipoOrfeo, $firmado = false) {

	//Empezando a radicar
	$this->establecerTipoRadicado($tipoOrfeo); //INFORMATIVO, ENTRADA, SALIDA
	//Si el radicado es de entrada y ya tiene un radicado se debe modificar.
	if ($tipoOrfeo == OrfeoWS::ENTRADA) {
	    $radicado = $objSolicitud->getRadicado();
	    if ($radicado != null) {
		throw new \Exception("La solicitud ya se encuentra radicada con " . $radicado);
	    }
	}

	//Si el radicado es de salida y ya tiene un radicado se debe modificar.
	if ($tipoOrfeo == OrfeoWS::SALIDA) {
	    $this->numRadicadoPadre = $objSolicitud->getRadicado();
	}

	$respuestaRadicacion = $this->radicarOrfeo($objSolicitud, $objSolicitante);

	//La respuesta en ocasiones se recibe como un string, se evidenció en los casos en que falla
	//por ello si llega un string json se parsea con un jsondecode
	if (is_string($respuestaRadicacion)) {
	    $tmpRespuestaRadicacion = json_decode($respuestaRadicacion);
	    $respuestaRadicacion = [];
	    foreach ($tmpRespuestaRadicacion as $key => $value) {
		$respuestaRadicacion[$key] = $value;
	    }
	}

	if ($respuestaRadicacion['success'] == 1) {
	    $trd = [
		'dependencia' => $objSolicitud->getTiposolicitud()->getSubdireccion()->getDependenciaorfeo(),
		'subdependencia' => $objSolicitud->getTiposolicitud()->getSubdependencia(),
		'serieDocumental' => $objSolicitud->getTiposolicitud()->getSeriedocumental(),
		'subserieDocumental' => $objSolicitud->getTiposolicitud()->getSubseriedocumental(),
		'tipoDocumental' => $objSolicitud->getTiposolicitud()->getTipodocumental(),
		'previsualizacion' => false
	    ];
	    //Configuramos la plantilla PDF
	    $dataDocumentoPlantilla = $this->crearArchivoPDF(
		    $classTemplate, $dataPdf, $trd, $respuestaRadicacion['numRadicado'],
		    //$objSolicitud->getTiposolicitud()->getNombre(),
		    $firmado, $objSolicitud->getIdsolicitud()
	    );


	    $this->extensionArchivo = 'pdf';
	    $this->archivo = DIRECTORY_SEPARATOR . "firmados" . DIRECTORY_SEPARATOR . $dataDocumentoPlantilla['archivo'];

	    //Adjuntamos el archivo
	    //Variable donde se encuentra el nombre de archivo a adjuntar
	    //$documento = [
	    //    'archivo' => $pdf->archivo,
	    //    'nombre' => $nombreTipoSolicitud
	    //];
	    //$ws->adjuntarDocumentos($documento);
	    $rtaPlantilla = $this->cargarPlantilla();
	    //error_log('Orfeo: ' . print_r($rtaPlantilla, true));
	    //error_log('Palnt: ' . print_r($dataDocumentoPlantilla, true));
	    if (!$rtaPlantilla['success']) {
		throw new \Exception("Error al cargar la plantilla. Archivo " . $dataDocumentoPlantilla['archivo']);
	    }

	    //Cerramos el radicado padre si es un orfeo de salida
	    if ($tipoOrfeo == OrfeoWS::SALIDA) {
		$this->numRadicado = $objSolicitud->getRadicado();
		$this->cerrarRadicado();
	    }

	    return [
		'fechaRadicadoOrfeo' => date('Y-m-d h:i:s'),
		'registroRadicadoOrfeo' => $respuestaRadicacion['numRadicado'],
		'urlConsulta' => $dataDocumentoPlantilla['urlOrfeo']
	    ];
	} else {
	    throw new \Exception("No ha sido posible crear el radicado en Orfeo.");
	    return null;
	}
    }

    private function crearArchivoPDF($classTemplate, $dataPdf, $trd, $numRadicado, $firmado = false, $idSolicitud = 0) {

	$rutaArchivo = '';
	$pdf = new $classTemplate(['container' => $this->container]);
	$pdf->setData(array_merge($dataPdf, $trd));
	$pdf->parametrizarPdf(); //

	$urlOrfeo = Parametro::obtener($this->container, 'url_consulta_orfeo') . $numRadicado;

	$pdf->urlConsulta = $urlOrfeo;
	$pdf->radicado = $numRadicado;

	$archivo = $pdf->archivo;

	//Generamos el PDF y nos devuelve la ruta absoluta del archivo
	$rutaArchivo = $pdf->obtenerPlantilla();

	if ($firmado) {

	    $em = $this->getDoctrine()->getEntityManager();
	    $rutaArchivo = $this->firmarDigitalmente($em, $idSolicitud, $rutaArchivo, $archivo);
	}

	return [
	    'archivo' => $archivo, //Nombre del archivo
	    'ruta' => $rutaArchivo, //Ruta completa del archivo
	    'urlOrfeo' => $urlOrfeo//URL del orfeo
	];
    }

    public function firmarDigitalmente($em, $idSolicitud, $rutaAchivo, $nombrearchivo) {
	$em = $this->getDoctrine()->getManager();
	$file = file_get_contents($rutaAchivo);

	$pdfFirmado = $this->get('saul.comun.firma')
		->firmar(
		$em, $this->getUser(), $idSolicitud, base64_encode($file)
	);

	if ($pdfFirmado['success'] == false) {
	    throw new \Exception("El documento no ha podido ser firmado digitalmente :(.");
	}

	$rutaSalida = WEB_DIRECTORY . DIRECTORY_SEPARATOR . "firmados" . DIRECTORY_SEPARATOR . $nombrearchivo;

	file_put_contents($rutaSalida, utf8_decode($pdfFirmado['archivo']));

	return $rutaSalida;
    }

    public function obtenerNumeroRadicadoOrfeo($objSolicitud, $objSolicitante, $tipoOrfeo) {
	//Empezando a radicar
	$this->establecerTipoRadicado($tipoOrfeo); //INFORMATIVO, ENTRADA, SALIDA
	//Si el radicado es de entrada y ya tiene un radicado se debe modificar.
	if ($tipoOrfeo == OrfeoWS::ENTRADA) {
	    $radicado = $objSolicitud->getRadicado();
	    if (!is_null($radicado)) {
		throw new \Exception("La solicitud ya tiene el radicado de entrada " . $radicado . " asignado.");
	    }
	}

	//Si el radicado es de salida y ya tiene un radicado se debe modificar.
	if ($tipoOrfeo == OrfeoWS::SALIDA) {
	    $radicado = $objSolicitud->getRadicadohijo();
	    if (!is_null($radicado)) {
		throw new \Exception("La solicitud ya tiene el radicado de respuesta " . $radicado . " asignado.");
	    }
	}

	//Verificamos si se desea crear o modificar el radicado.
	//$respuestaRadicacion = $this->radicarOrfeo($objSolicitud, $objSolicitante);
	$respuestaRadicacion = $this->radicarOrfeo($objSolicitud, $objSolicitante);

	//La respuesta en ocasiones se recibe como un string, se evidenció en los casos en que falla
	//por ello si llega un string json se parsea con un jsondecode
	if (is_string($respuestaRadicacion)) {
	    $tmpRespuestaRadicacion = json_decode($respuestaRadicacion);
	    $respuestaRadicacion = [];
	    foreach ($tmpRespuestaRadicacion as $key => $value) {
		$respuestaRadicacion[$key] = $value;
	    }
	}

	if ($respuestaRadicacion['success'] == 1) {
	    return $respuestaRadicacion['numRadicado'];
	} else {
	    return null;
	}
    }

}

?>
