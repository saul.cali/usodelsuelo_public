<?php

namespace saul\ComunBundle\Classes;

/**
 * jqGridBase
 *
 * 
 * Configuracion Base del Objeto jqGrid
 *
 *
 * @package    saul
 * @author     Alejandro Ordoñez Del Valle
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class DataTableBase
{

    private $total = null,
            $pagina = null,
            $limite = 0,
            $numRegistros = null,
            $inicio = 0,
            $ordenarPor = null,
            $ordenarHacia = null;

    /* public function __construct($draw, $ordenarPor, $ordenarHacia) {
      $this->draw             =   $draw;
      $this->ordenarPor       =   $ordenarPor;
      $this->ordenarHacia     =   $ordenarHacia;
      }

      /**
     * Calcular y establacer el total de páginas, la página actual y el limite inicial
     */

    public function establecerParametros()
    {
        if (!$this->ordenarPor)
            $this->ordenarPor = 1;

        if ($this->numRegistros > 0)
        {
            //$this->total = ceil($this->numRegistros/$this->limite);
        } else
        {
            $this->total = 0;
        }
        if ($this->pagina > $this->total)
            $this->pagina = $this->total;
        $this->inicio = $this->limite * $this->pagina - $this->limite; //do not put $limit*($page - 1)
        if ($this->inicio < 0)
            $this->inicio = 0;
    }

    /**
     * Obtener la página actual del Grid
     * @return integer
     */

    /**
     * Obtener el número total de páginas del Grid
     * @return integer
     */
    public function obtenerTotal()
    {
        return $this->total;
    }

    /**
     * Obtener el número de registros mostrados en el Grid
     * @return integer
     */
    public function obtenerNumRegistros()
    {
        return $this->numRegistros;
    }

    /**
     * Obtener el valor del limite final
     * @return integer
     */
    public function obtenerLimite()
    {
        return $this->limite;
    }

    /**
     * Obtener el valor del limite inicial
     * @return integer
     */
    public function obtenerInicio()
    {
        return $this->inicio;
    }

    /**
     * Establecer valor a numRegistros
     * @param type $numRegistros
     */
    public function establecerNumRegistros($numRegistros)
    {
        $this->numRegistros = $numRegistros;
    }

    /**
     * Establecer el idioma del DataTable
     * @param type $lg
     * @return string
     */
    static function establecerIdioma($lg = 'ES')
    {
        switch ($lg)
        {
            case 'ES':
                $datos = '{
                                "lengthMenu": "<div class=\"total_paginas_DT total_paginas_DT_l\">Mostrar</div><div class=\"total_paginas_DT total_paginas_DT_c\">_MENU_</div><div class=\"total_paginas_DT total_paginas_DT_r\">registros</div>",
                                "zeroRecords": "No se ha encontrado información",
                                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                                "infoEmpty": "No hay registros disponibles",
                                "infoFiltered": "(Filtrando de _MAX_ registros totales)",
                                "emptyTable":     "No hay informacion disponible en la tabla",
                                "infoPostFix":    "",
                                "thousands":      ".",
                                "loadingRecords": "Cargando...",
                                "processing":     " ",
                                "search":         "Buscar:",
                                "paginate": {
                                    "first":      "Primero",
                                    "last":       "Último",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                                "aria": {
                                    "sortAscending":  ": ordenar ascendente",
                                    "sortDescending": ": ordenar descendente"
                                }
                              }';
                break;
        }
        return $datos;
    }

    static function establecerDOM()
    {
        return '\'rt<"table-footer clearfix"<"DT-label col-xs-6 col-sm-4"i><"DT-total col-xs-6 col-sm-3"l><"DT-pagination DT-lf-right"p>>\'';
    }

    static function establecerPaginacion()
    {
        return '"simple_numbers"';
    }

    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL limit clause
     */
    static function limit($request, $columns)
    {
        $limit = '';
        $start = $request->query->get('start', NULL);
        $length = $request->query->get('length', NULL);
        if (($start != NULL) && (($length != -1) OR ( $length != NULL) ))
        {
            $limit = " LIMIT " . intval($request->query->get('start')) . ", " . intval($request->query->get('length'));
        }
        return $limit;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     *  @return string SQL order by clause
     */
    static function order($request, $columns)
    {
        $order = $request->query->get('order', false);
        if ($order && count($order))
        {
            $orderBy = array();
            $dtColumns = self::pluck($columns, 'dt');
            for ($i = 0, $ien = count($order); $i < $ien; $i++)
            {
                // Convert the column index into the column data property
                $columnIdx = intval($order[$i]['column']);
                $requestColumn = $request->query->get('columns')[$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true')
                {
                    $dir = $order[$i]['dir'] === 'asc' ?
                            'ASC' :
                            'DESC';
                    $orderBy = array('0' => $column['db'], '1' => $dir);
                    //$orderBy[] = $column['db'];
                }
            }
            //$order = implode(', ', $orderBy);
            $order = $orderBy;
        }
        return $order;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     *  @param  array $request Data sent to server by DataTables
     *  @param  array $columns Column information array
     */
    static function filtrar($request, $columnas)
    {
        $filtro = array();
        $dtColumnas = self::pluck($columnas, 'dt');
        $requestColumnas = $request->query->get('columns', array());
        $search = $request->query->get('search', null);
        $value = $request->query->get('search[value]');

        if (!is_null($search) && $value != '')
        {
            foreach ($requestColumnas as $columna => $val)
            {
                $requestColumna = $columna[$val];
                $columnIdx = array_search($requestColumna['data'], $dtColumnas);
                $columna = $columnas[$columnIdx];
                if ($requestColumna['searchable'] == 'true')
                {
                    $filtro['filtro'][] = array(
                        "nombre" => $columna['db'],
                        "valor" => "%" . $value . "%"
                    );
                }
            }
            $filtro['OR'] = true;
        }
        foreach ($requestColumnas as $requestColumna)
        {
            $columnIdx = array_search($requestColumna['data'], $dtColumnas);
            $columna = $columnas[$columnIdx];
            $value = $requestColumna['search']['value'];
            if ($requestColumna['searchable'] == 'true' && $value != '')
            {
                $filtro['filtro'][] = array(
                    "nombre" => $columna['db'],
                    "valor" => $columna['regex'] ? "%" . $value . "%" : $value,
                    "regex" => $columna['regex']
                );
                $filtro['OR'] = false;
            }
        }
        return $filtro;
    }

    /**
     * Pull a particular property from each assoc. array in a numeric array, 
     * returning and array of the property values from each item.
     *
     *  @param  array  $a    Array to get data from
     *  @param  string $prop Property to read
     *  @return array        Array of property values
     */
    static function pluck($a, $prop)
    {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++)
        {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

    /**
     * Create the data output array for the DataTables rows
     *
     *  @param  array $columns Column information array
     *  @param  array $data    Data from the SQL get
     *  @return array          Formatted data in a row based format
     */
    static function dataOutput($columns, $data)
    {
        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++)
        {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++)
            {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter']))
                {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else
                {
                    $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

}

?>
