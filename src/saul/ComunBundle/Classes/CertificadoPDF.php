<?php

namespace saul\ComunBundle\Classes;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use saul\ComunBundle\Classes\Parametro;
/**
 * CertificadoPDF
 *
 * 
 * Generar el certifcado Registro en PDF 
 *
 *
 * @package    ruva
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */

class CertificadoPDF extends \TCPDF { 

    private $periodoPermiso     =   null;
    public $fechaHoy            =   null;
    public $radicadoPadre       =   null;
    public $preparadoPor        =   null;
    public $dependencia         =   null;    //
    public $subdependencia      =   null;
    public $serie               =   null;        //Procesos
    public $subserie            =   null;       //PROCESO A LA PUBLICIDAD EXTERIOR VISUAL
    public $tipoSoporte         =   "2";        //ELECTRONICO
    public $tipoDocumental      =   null;
    public $consecutivo         =   null;
    public $nombreUsuario       =   null;
    public $nombreEmpresa       =   null;
    public $radicado            =   null;
    public $solicitud           =   null;
    public $fechaRadicado       =   null;
    public $nombreSubdirector      =   null;
    public $numIdentificacion   =   null;
    public $archivo             =   null;
    public $dirImagenes         =   null;
    public $firmaMecanica       =   null;
    protected $logo             =   "logo_alcaldia_dapm_sm.png";
    public $urlQR               =   null;
    protected $dirDocumentos    =   null;
    public $salidaArchivo       =   'F'; /**Para el uso de la salida del documento, parametro de la funcion Output Clase TCPDF**/

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false,  $container) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa, $container);
        
        $this->container = $container;
        $this->archivo          =   sha1(date("d-m-Y H:i:s")) . '.pdf';
        //$this->container = new ContainerBuilder();
        $this->dirImagenes      =   $this->container->get('kernel')->getRootDir(). DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
        $this->dirDocumentos    =   $this->container->get('kernel')->getRootDir(). DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
        $this->fechaHoy         =   date("d-m-Y");
        $this->periodoPermiso   =   date("Y");
        //$this->urlQR            =   $request->getPathInfo().'/solicitud/consultarradicado/radicado/';
//        $this->urlQR            =   sfContext::getInstance()->getRequest()->getUriPrefix() .
//                                    sfContext::getInstance()->getRequest()->getRelativeUrlRoot() .                                    
//                                    '/solicitud/consultarradicado/radicado/';        
        $this->dependencia = Parametro::obtener($this->container, 'dependencia_ws_orfeo');
    }
    /**
     * Crea la cabecera del archivo
     */
    public function Header() {
        // Logo        
        $image_file = $this->dirImagenes . $this->logo;
        //$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
        $this->Image($image_file, 7, 7, 30, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'M', 'M');        
    }

    /**
     * Crea el pie de pagina
     */
    public function Footer() {
        // Posicion 28 mm desde el bottom
        $this->SetY(-28);
        // Set font
        $this->SetFont('helvetica', 'I', 8);

        // texto del pie de pagina
        $this->Cell(0,4,"Centro Administrativo Municipal Torre Alcaldia.Piso 10",0,1,'C'); 
        $this->Cell(0,4,"Telefono 668 91 00. Fax 8895630",0,1,'C'); 
        $this->Cell(0,4,"www.cali.gov.co",0,1,'C'); 
    }
    
    /**
    * Inserta una marca de agua en el documento
    */
    public function insertarMarcadeagua($imagen)
    {
        // -- set new background ---

        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->getAutoPageBreak();
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        //$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
        $this->Image($this->dirImagenes . $imagen, 2, 2, 206, 293, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        //$this->setPageMark();
    }
    
    
    public function parametrizarPdf(){

           //$pdf = new certificadoReg(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

       //set margins
       $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
       $this->SetHeaderMargin(PDF_MARGIN_HEADER);
       $this->SetFooterMargin(PDF_MARGIN_FOOTER);
        //rompimiento de pagina automatico
       $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
       //set image scale factor
       $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
       //$this->setLanguageArray($l);
        // ---------------------------------------------------------

        // set font estilo de letra 
       $this->SetFont('times', '', 12);
        // add una pagina para luego mostrar datos 
       $this->AddPage();
    }

 public function texto1Pdf(){
     // cuerpo del documento en html el cual se imprime en el pdf
//aqui se encuentran las varibles que cambian ej $Radicado etc..
$this->establecerEstiloCodBarras();
$this->escribirCodBarras(138,20,50,8, 50);
$html1 = <<<EOD
<p align="right">
    <strong>Al contestar por favor cite estos datos:</strong><br />
  Radicado No.: <strong>$this->radicado</strong><br />
  Fecha: <strong>$this->fechaHoy</strong><br />
  TRD: <strong>$this->dependencia.$this->serie.$this->subserie.$this->tipoDocumental.$this->consecutivo</strong><br />
  Rad.  Padre:<strong>$this->radicadoPadre</strong></p>

  <p>Santiago de Cali, $this->fechaHoy </p>
<p>&nbsp;&nbsp;Se&ntilde;or(a)(es):<br>
    <strong>$this->nombreEmpresa</strong><br>
  Representante legal<br>
  $this->nombreUsuario</p>
<p>Asunto: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Certificado  de Registro para desarrollar la actividad de la publicidad&nbsp; <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Exterior. Radicado $this->radicadoPadre  del  $this->fechaRadicado</p>
<p>Cordial saludo.</p>
<p align="justify">Anexo a la presente el <strong>CERTIFICADO DE REGISTRO PARA DESARROLLAR LA ACTIVIDAD DE LA PUBLICIDAD EXTERIOR VISUAL EN  EL MUNICIPIO DE SANTIAGO DE CALI, </strong>de  acuerdo a su solicitud.<br>
Le remito igualmente el comunicado  informativo, recordando que los propietarios de las vallas o elementos  estructurales, los anunciantes y los propietarios de los predios donde se  ubican las mismas, son responsables solidarios en el pago de las sanciones y  dem&aacute;s cobros que lleguen a imputarse por parte de la Administraci&oacute;n Municipal  por la instalaci&oacute;n de vallas en violaci&oacute;n al Acuerdo Municipal 0179 de 2006. <br>
  <br>
Se anexa copia del Decreto No. 411.0.20.0858 expedido por el Se&ntilde;or Alcalde el  d&iacute;a 23 de Noviembre de 2012, &ldquo;POR MEDIO DEL CUAL SE ADOPTAN MEDIDAS PARA  CONTROLAR LA CONTAMINACI&Oacute;N POR ELEMENTOS DE PUBLICIDAD EXTERIOR VISUAL Y SE  DICTAN OTRAS DISPOSICIONES&rdquo;. 

<p>Atentamente.</p>
<p></p>
<p></p>
<p><strong>$this->nombreSubdirector</strong><br>
Subdirector(a) de Ordenamiento  Urban&iacute;stico.</p>
<p>&nbsp;</p>
<p> </p>
EOD;
$this->Image($this->firmaMecanica, 10, 204, 60, '', '', '', '', false, 300);
$this->writeHTMLCell($w=175, $h=12, $x='', $y='', $html1, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$this->lastPage();
$this->AddPage();

 }
 public function texto2Pdf() {
    $html2 = <<<EOD
    <p> </p>
    <p>Santiago de Cali, $this->fechaHoy</p>
    <p> </p>
    <p align="center"><strong>CERTIFICADO  DE REGISTRO PARA DESARROLLAR LA ACTIVIDAD DE LA PUBLICIDAD EXTERIOR VISUAL EN  EL MUNICIPIO DE SANTIAGO DE CALI</strong></p>
    <p align="center"><strong>&nbsp;EL&nbsp;  SUSCRITO SUBDIRECTOR&nbsp; DE  ORDENAMIENTO URBANISTICO DEL DEPARTAMENTO ADMINISTRATIVO DE PLANEACION  MUNICIPAL</strong></p>
    <p align="center"><strong>CERTIFICA</strong></p><br>
    <p align="justify">Que  de conformidad con el art&iacute;culo 5&ordm;, par&aacute;grafo 3&ordm;, del Acuerdo No. 179 de 2006,  &ldquo;Por medio del cual se reglamenta la publicidad exterior visual mayor, menor y  avisos en el Municipio de 
      Santiago de Cali y se dictan otras disposiciones&rdquo;, la  empresa&nbsp; <strong>$this->nombreEmpresa </strong>identificada con Nit: $this->numIdentificacion se encuentra  registrada para desarrollar la actividad de&nbsp;  la publicidad exterior visual en el Municipio de Santiago de Cali. </p>
    <p>Para  lo cual adjuntaron con radicado No.$this->radicadoPadre del $this->fechaRadicado, la siguiente informaci&oacute;n:</p>
    <p align="justify">
    <ul type="disc">
      <li>Solicitud       por escrito de registro de la actividad por parte del representante legal       de la sociedad antes anotada.</li>
      <li>Registro       vigente ante la C&aacute;mara de Comercio de Cali, donde consta el objeto social       o actividad.</li>
      <li>Certificado       de paz y salvo&nbsp; del impuesto de       Industria y comercio, expedido por el Departamento Administrativo de       hacienda Municipal. </li>
      <li>Certificado       &nbsp;expedido por la Subsecretar&iacute;a de       Convivencia y seguridad Ciudadana, en la cual consta que no ha sido       sancionado su empresa por violaci&oacute;n a las normas de Espacio P&uacute;blico       durante el a&ntilde;o $this->periodoPermiso.</li>
    </ul></p>

    <p>&nbsp;</p>
    <p align="center"><strong>$this->nombreSubdirector</strong><br>
    Subdirector(a) &nbsp;de Ordenamiento Urban&iacute;stico</p>
EOD;
    //$this->Image($this->firmaMecanica, 70, 202, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
    $this->Image($this->firmaMecanica, 70, 204, 60, '', '', '', '', false, 300);
    //preparamos el documento con todos los datos 
    $this->writeHTMLCell($w=175, $h=12, $x='', $y='', $html2, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

    // ---------------------------------------------------------
    // QRCODE,L : QR-CODE Low error correction aqui es el codigo QR del radicado

     $this->write2DBarcode($this->urlQR . $this->radicado, 'QRCODE,H', 163, 210, 27, 50, '', 'N');
     $this->Output($this->dirDocumentos . $this->archivo, 'F');
 }
 
 public function crearPlantilla()
 {
    $this->texto1Pdf();
    $this->texto2Pdf();
 }
 
  /**
  * establece los estiulos del codigo de barras lineal
  */
 protected function establecerEstiloCodBarras()
 {
    $this->estiloCodigoBarras = array( 
        'font' => 'helvetica', 
        'fontsize' => 10, 
    ); 
     
 }
 /**
  * Escribe el codigo de barras del radicado en la plantilla
  * @param type $x
  * @param type $y
  * @param type $ancho
  * @param type $alto
  * @param type $xres
  */
 protected function escribirCodBarras($x, $y, $ancho, $alto, $xres)
 {
     //$this->write1DBarcode('2014413230086891','MSI+',55,20,50,14,'',$this->estiloCodigoBarras);
     $this->write1DBarcode($this->radicado,'MSI+',$x, $y, $ancho, $alto, $xres, $this->estiloCodigoBarras);
 }
 
     /**
     * INserta un marco con bordes redondeados
     * @param type $x
     * @param type $y
     * @param type $w
     * @param type $h
     */
    protected function insertarMarco($x = 5, $y = 5, $w = 20, $h = 10) {
        $this->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        // ($x, $y, $w, $h, $r, $round_corner='1111', $style='', $border_style=array(), $fill_color=array())
        $this->RoundedRect($x, $y, $w, $h, 1, '1111', '');
    }

 
 /**
  * Establece la informacion del encabezado de radicacion
  * @param Integer $y
  */
 protected function establecerEncabezadoRadicacion($y = 0, $x = 0)
    {
        $this->establecerEstiloCodBarras();
        $this->escribirCodBarras(153-$x, $y+7, 50, 8, 60);
        // Set font
        $this->SetFont('helvetica', 'A', 9);
        $this->SetY($y+16);
        $this->SetX(153-$x);
        $this->Cell($w=50, $h=0, '*' . $this->radicado . '*', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=4, $ignore_min_height=false, $calign='T', $valign='M');
        $this->SetY($y+20);
        $this->SetX(153-$x);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell($w=50, $h=0, 'Al contestar por favor cite estos datos:', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        
        $this->SetY($y+24);
        $this->SetX(172-$x);
        $this->SetFont('helvetica', 'A', 9);
        $this->Cell($w=20, $h=0, 'Solicitud No: ', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        $this->SetX(178-$x);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell($w=25, $h=0, $this->solicitud, $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        
        $this->SetY($y+28);
        $this->SetX(158-$x);
        $this->SetFont('helvetica', 'A', 9);
        $this->Cell($w=15, $h=0, 'Radicado No: ', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        $this->SetX(178-$x);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell($w=25, $h=0, $this->radicado, $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        
        $this->SetY($y+32);
        $this->SetX(171-$x);
        $this->SetFont('helvetica', 'A', 9);
        $this->Cell($w=15, $h=0, 'Fecha: ', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        $this->SetX(178-$x);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell($w=25, $h=0, $this->fechaHoy, $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        
        $this->SetY($y+36);
        $this->SetX(150);
        $this->SetFont('helvetica', 'A', 9);
        $this->Cell($w=15, $h=0, 'TRD: ', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        $this->SetX(178-$x);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell($w=25, $h=0, $this->dependencia . '.' . $this->subdependencia . '.' . $this->serie . '.' . $this->subserie . '.' . $this->tipoDocumental . '.' . $this->consecutivo, $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        
        if ($this->radicadoPadre)
        {
            $this->SetY($y+40);
            $this->SetX(158-$x);
            $this->SetFont('helvetica', 'A', 9);
            $this->Cell($w=15, $h=0, 'Radicado Padre: ', $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
            $this->SetX(178-$x);
            $this->SetFont('helvetica', 'B', 9);
            $this->Cell($w=25, $h=0, $this->radicadoPadre, $border=0, $ln=0, $align='R', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
        }
    }
    
    protected function generarEncabezadoCalidad($nombre="", $version="",$fechaentradavigencia="", $codigoformato="")
    {
        
        $html=<<<EOD
        <table border="1" cellspacing="0" cellpadding="1" width="640">
          <tr>
            <td width="120" rowspan="3"><p align="center">&nbsp;</p>
                <p align="center">&nbsp;</p></td>
            <td width="400" rowspan="3"><p align="center"><br>SISTEMAS DE GESTIÓN Y CONTROL INTEGRADOS <br>
                                            (SISTEDA, SGC y MECI)</p>
                <p align="center"><strong>$nombre<br></strong><strong> </strong></p></td>
            <td width="188" height="50" colspan="2"><p align="center"><br>$codigoformato </p></td>
          </tr>
          <tr>
            <td width="97"><p align="center">VERSIÓN</p></td>
            <td width="91"><p align="center">$version</p></td>
          </tr>
          <tr>
            <td width="97"><p align="center">FECHA ENTRADA EN VIGENCIA</p></td>
            <td width="91"><p align="center"><br>$fechaentradavigencia</p></td>
          </tr>
        </table>


EOD;
        $this->SetFont('helvetica', 'B', 9);
        $this->writeHTMLCell($w = 175, $h = 12, $x = 5, $y = 4, $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    }
    
    /**
     * Obtener el nombre del archivo generado
     * @return string
     */
    public function obtenerNombreArchivo()
    {
        return $this->archivo;
    }
    
    public function establecerTituloNoEncabezado($x = 0, $y = 0, $titulo ="", $fuente = '', $formatoTexto = '', $tamano = 9)
    {
        $this->SetFont($fuente, $formatoTexto, $tamano);
        $this->writeHTMLCell($w='', $h='', $x, $y, $titulo, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
    }
    
    public function establecerImagen($imagen, $x, $y, $ancho, $alto)
    {
        if ($imagen)
        {
            $this->Image($this->dirDocumentos . $imagen, $x, $y, $ancho, $alto, '', '', 'T', true, 700, '', false, false, 0, false, false, false);
        } else
        {
            $this->Image($this->dirImagenes . Parametro::obtener('image_default'), $x, $y, $ancho, $alto, '', '', 'T', true, 700, '', false, false, 0, false, false, false);
        }
    }
            
}
?>
