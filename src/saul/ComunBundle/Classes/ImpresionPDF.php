<?php

namespace saul\ComunBundle\Classes;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * ImpresionPDF
 *
 * 
 * Generar el archivo en PDF 
 *
 *
 * @package    ruva
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ImpresionPDF extends \TCPDF {

    public $fechaHoy = null;
    public $archivo = null;
    public $dirImagenes = null;
    public $firmaMecanica = null;
    protected $logo = "logo_alcaldia_dapm_sm.png";
    public $urlQR = null;
    protected $dirDocumentos = null;
    public $salidaArchivo = 'F'; /*     * Para el uso de la salida del documento, parametro de la funcion Output Clase TCPDF* */

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $container) {
	parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa, $container);

	$this->container = $container;
	$this->archivo = sha1(date("d-m-Y H:i:s")) . '.pdf';
	$this->dirImagenes = $this->container->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
	$this->dirDocumentos = $this->container->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
	$this->fechaHoy = date("d-m-Y");
	$this->periodoPermiso = date("Y");
    }

    /**
     * Crea la cabecera del archivo
     */
    public function Header() {
	// Logo        
	$image_file = $this->dirImagenes . $this->logo;
	//$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
	$this->Image($image_file, 7, 7, 30, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
	// Set font
	$this->SetFont('helvetica', 'B', 20);
	// Title
	$this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    /**
     * Crea el pie de pagina
     */
    public function Footer() {
	// Posicion 28 mm desde el bottom
	$this->SetY(-28);
	// Set font
	$this->SetFont('helvetica', 'I', 8);
	// texto del pie de pagina
	$this->Cell(0, 4, "Centro Administrativo Municipal Torre Alcaldia.Piso 10", 0, 1, 'C');
	$this->Cell(0, 4, "Telefono 668 91 00. Fax 8895630", 0, 1, 'C');
	$this->Cell(0, 4, "www.cali.gov.co", 0, 1, 'C');
    }

    /**
     * Inserta una marca de agua en el documento
     */
    public function insertarMarcadeagua($imagen) {
	// -- set new background ---
	// get the current page break margin
	$bMargin = $this->getBreakMargin();
	// get current auto-page-break mode
	$auto_page_break = $this->getAutoPageBreak();
	// disable auto-page-break
	$this->SetAutoPageBreak(false, 0);
	// set bacground image
	//$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
	$this->Image($this->dirImagenes . $imagen, 2, 2, 206, 293, '', '', '', false, 300, '', false, false, 0);
	// restore auto-page-break status
	$this->SetAutoPageBreak($auto_page_break, $bMargin);
	// set the starting point for the page content
	//$this->setPageMark();
    }

    public function parametrizarPdf() {

	//$pdf = new certificadoReg(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	//set margins
	$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$this->SetHeaderMargin(PDF_MARGIN_HEADER);
	$this->SetFooterMargin(PDF_MARGIN_FOOTER);
	//rompimiento de pagina automatico
	$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	//set image scale factor
	$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
	//set some language-dependent strings
	//$this->setLanguageArray($l);
	// ---------------------------------------------------------
	// set font estilo de letra 
	$this->SetFont('times', '', 12);
	// add una pagina para luego mostrar datos 
	$this->AddPage();
    }

    public function crearPlantilla() {
	$this->texto1Pdf();
	$this->texto2Pdf();
    }

    /**
     * establece los estiulos del codigo de barras lineal
     */
    protected function establecerEstiloCodBarras() {
	$this->estiloCodigoBarras = array(
	    'font' => 'helvetica',
	    'fontsize' => 10,
	);
    }

    /**
     * Escribe el codigo de barras del radicado en la plantilla
     * @param type $x
     * @param type $y
     * @param type $ancho
     * @param type $alto
     * @param type $xres
     */
    protected function escribirCodBarras($x, $y, $ancho, $alto, $xres) {
	//$this->write1DBarcode('2014413230086891','MSI+',55,20,50,14,'',$this->estiloCodigoBarras);
	$this->write1DBarcode($this->radicado, 'MSI+', $x, $y, $ancho, $alto, $xres, $this->estiloCodigoBarras);
    }

    /**
     * INserta un marco con bordes redondeados
     * @param type $x
     * @param type $y
     * @param type $w
     * @param type $h
     */
    protected function insertarMarco($x = 5, $y = 5, $w = 20, $h = 10) {
	$this->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
	// ($x, $y, $w, $h, $r, $round_corner='1111', $style='', $border_style=array(), $fill_color=array())
	$this->RoundedRect($x, $y, $w, $h, 1, '1111', '');
    }

    /**
     * Obtener el nombre del archivo generado
     * @return string
     */
    public function obtenerNombreArchivo() {
	return $this->archivo;
    }

    public function establecerTituloNoEncabezado($x = 0, $y = 0, $titulo = "", $fuente = '', $formatoTexto = '', $tamano = 9) {
	$this->SetFont($fuente, $formatoTexto, $tamano);
	$this->writeHTMLCell($w = '', $h = '', $x, $y, $titulo, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    }

    public function establecerImagen($imagen, $x, $y, $ancho, $alto) {
	if ($imagen) {
	    $this->Image($this->dirDocumentos . $imagen, $x, $y, $ancho, $alto, '', '', 'T', true, 700, '', false, false, 0, false, false, false);
	} else {
	    $this->Image($this->dirImagenes . Parametro::obtener('image_default'), $x, $y, $ancho, $alto, '', '', 'T', true, 700, '', false, false, 0, false, false, false);
	}
    }

}

?>
