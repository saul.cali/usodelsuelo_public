<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Barrio
 *
 * @ORM\Table(name="ciudad")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\CiudadRepository")
 */
class Ciudad
{
    /**
     * @var int
     *
     * @ORM\Column(name="idciudad", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idCiudad;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="codigopais", type="string", length=4, nullable=true)
     */
    private $codigoPais;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getIdCiudad()
    {
        return $this->idCiudad;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Barrio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set nombre
     *
     * @param string $codigoPais
     *
     * @return Barrio
     */
    public function setCodigoPais($codigoPais)
    {
        $this->codigoPais = $codigoPais;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigoPais()
    {
        return $this->codigoPais;
    }
    
    
    /**
     * @ORM\ManyToOne(targetEntity="\saul\ComunBundle\Entity\Pais")
     * @ORM\JoinColumn(name="codigoPais", referencedColumnName="codigo")
     */
    private $pais;
    
    public function setPais(\saul\ComunBundle\Entity\Pais $pais)
    {
        $this->pais  = $pais;
        $this->codigoPais = $pais->codigo();
    }
    
    public function getPais()
    {
        return $this->pais;
    }
}

