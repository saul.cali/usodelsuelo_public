<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estado
 *
 * @ORM\Table(name="estado")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\EstadoRepository")
 */
class Estado {

    const
	    PENDIENTE = "pendiente",
	    PREAPROBADA = "preaprobada",
	    APROBADA = "aprobada",
	    DENEGADA = "denegada",
	    PENDIENTE_POR_VISITA = "pendientevisita",
	    VISITADA = "visitada",
	    REVISION = "revision",
	    PENDIENTE_POR_PAGAR = "pendienteporpagar",
	    PAGADA = "pagada",
	    PRELIQUIDADA = "preliquidada",
	    PENDIENTE_POR_DOCS = "pendientedocs",
	    ANULADA = "anulada",
	    PROCESADO = "procesado",
	    PERMITIDO = "permitido",
	    NO_PERMITIDO = "nopermitido",
	    MIGRADO = "migrado",
	    TIPO_ABIERTO = "abierto",
	    PRE_PERMITIDO = "prepermitido",
	    EMITIDO = "emitido",
	    PENDIENTE_VISITA_POT = "pendientevisitapot",
	    PENDIENTE_GESTION_POT = "pendientegestionpot",
	    PENDIENTE_APROBACION_POT = "pendienteaprobacionpot",
	    PENDIENTE_REVISION_POT = "pendienterevisionpot",
	    PENDIENTE_POR_INFORMACION = "pendienteporinformacion",
	    PENDIENTE_RELACION_CIIU = "pendienterelacionciiu",
	    PLAN_PARCIAL = "planparcial",
	    VIABLE = "viable",
	    EN_GESTION = "engestion",
	    PENDIENTE_POR_VERIFICAR = "pendienteverificar",
	    VERIFICADA = "verificada",
	    POR_VERIFICAR = "porverificar",
	    POR_INVESTIGAR = "porinvestigar",
	    PENDIENTE_POR_REVISION = "pendienterevision",
	    CERTIFICADA = "certificada",
	    PENDIENTE_POR_INFORMACION_CIUDADANO = "pendienteporinformacionciudadano",
	    COMPLETADA = "completada",
	    RADICACION_PARCIAL = "radicacionparcial",
	    PENDIENTE_PLANTILLA_ORFEO = "pendienteplantillaorfeo",
	    PENDIENTE_ANEXOS_ORFEO = "pendienteanexosorfeo",
	    PRERADICADA = "preradicada"

    ;

    /**
     * @var int
     *
     * @ORM\Column(name="idestado", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idestado;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=45, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=10, nullable=true)
     */
    private $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdestado() {
	return $this->idestado;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Estado
     */
    public function setCodigo($codigo) {
	$this->codigo = $codigo;

	return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo() {
	return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Estado
     */
    public function setNombre($nombre) {
	$this->nombre = $nombre;

	return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
	return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Estado
     */
    public function setDescripcion($descripcion) {
	$this->descripcion = $descripcion;

	return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
	return $this->descripcion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Estado
     */
    public function setTipo($tipo) {
	$this->tipo = $tipo;

	return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
	return $this->tipo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Estado
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Estado
     */
    public function setUpdatedAt($updatedAt) {
	$this->updatedAt = $updatedAt;

	return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
	return $this->updatedAt;
    }

}
