<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoDocIdentificacion
 *
 * @ORM\Table(name="tipodocidentificacion")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\TipoDocIdentificacionRepository")
 */
class TipoDocIdentificacion
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="idtipodocidentificacion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idTipoDocIdentificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
	
	/**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=45)
     */
    private $tipo;
	
	/**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=75)
     */
    private $descripcion;
	
	/**
     * @var string
     *
     * @ORM\Column(name="numdigitos", type="integer")
     */
    private $numDigitos;
       
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    /**
     * Get id
     *
     * @return int
     */
    public function getIdTipoDocIdentificacion()
    {
        return $this->idTipoDocIdentificacion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoCampo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
	
	/**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return TipoDocIdentificacion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
  
	/**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Estado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

	/**
     * Set numdigitos
     *
     * @param string $numDigitos
     *
     * @return TipoDocIdentificacion
     */
    public function setNumDigitos($numDigitos)
    {
        $this->numDigitos = $numDigitos;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getNumDigitos()
    {
        return $this->numDigitos;
    }
	
	/**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Estado
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Estado
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

