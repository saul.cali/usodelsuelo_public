<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCampo
 *
 * @ORM\Table(name="pnitem")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\PnItemRepository")
 */
class PnItem
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="iditem", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idItem;

    /**
     * @var string
     *
     * @ORM\Column(name="estandar", type="string", length=255)
     */
    private $estandar;
   
    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;
  
    /**
     * @var date
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var date
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    /**
     * Set idItem
     *
     * @param int $idItem
     *
     * @return PnItem
     */
    public function setIdItem($idItem)
    {
        $this->idItem = $idItem;

        return $this;
    }

    /**
     * Set estandar
     *
     * @param string $estandar
     *
     * @return PnItem
     */
    public function setEstandar($estandar)
    {
        $this->estandar = $estandar;

        return $this;
    }

    /**
     * Get estandar
     *
     * @return string
     */
    public function getEstandar()
    {
        return $this->estandar;
    }
    
     /**
     * Set texto
     *
     * @param string $texto
     *
     * @return PnItem
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }
  
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Barrio
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Barrio
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}

