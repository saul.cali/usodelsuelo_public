<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prueba
 *
 * @ORM\Table(name="prueba")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\PruebaRepository")
 */
class Prueba
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_at", type="datetime")
     */
    private $fechaAt;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre_id", type="integer")
     */
    private $nombreId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Prueba
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaAt
     *
     * @param \DateTime $fechaAt
     *
     * @return Prueba
     */
    public function setFechaAt($fechaAt)
    {
        $this->fechaAt = $fechaAt;

        return $this;
    }

    /**
     * Get fechaAt
     *
     * @return \DateTime
     */
    public function getFechaAt()
    {
        return $this->fechaAt;
    }

    /**
     * Set nombreId
     *
     * @param integer $nombreId
     *
     * @return Prueba
     */
    public function setNombreId($nombreId)
    {
        $this->nombreId = $nombreId;

        return $this;
    }

    /**
     * Get nombreId
     *
     * @return int
     */
    public function getNombreId()
    {
        return $this->nombreId;
    }
}

