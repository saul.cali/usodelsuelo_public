<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCampo
 *
 * @ORM\Table(name="tipoestado")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\TipoEstadoRepository")
 */
class TipoEstado
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="idtipoestado", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idTipoEstado;

    
    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50)
     */
    private $codigo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
   

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=50)
     */
    private $descripcion;
   
     /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="text", length=50)
     */
    private $tipo;
    
      /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
   
    /**
     * Get id
     *
     * @return int
     */
    public function getIdTipoEstado()
    {
        return $this->idTipoEstado;
    }
    
    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Comuna
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoCampo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    
    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoEstado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    
    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return TipoEstado
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    
     /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Comuna
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Comuna
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  
}

