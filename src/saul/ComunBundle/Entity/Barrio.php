<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Barrio
 *
 * @ORM\Table(name="barrio")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\BarrioRepository")
 */
class Barrio
{
    /**
     * @var int
     *
     * @ORM\Column(name="idbarrio", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idbarrio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=4, nullable=true)
     */
    private $codigo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="codigoes", type="string", length=2, nullable=true)
     */
    private $codigoes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="idcomuna", type="integer", nullable=true)
     */
    private $idcomuna;


    /**
     * Get id
     *
     * @return int
     */
    public function getIdbarrio()
    {
        return $this->idbarrio;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Barrio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set nombre
     *
     * @param string $codigo
     *
     * @return Barrio
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    
    /**
     * Set nombre
     *
     * @param string $codigoes
     *
     * @return Barrio
     */
    public function setCodigoes($codigoes)
    {
        $this->codigo = $codigoes;

        return $this;
    }

    /**
     * Get codigoes
     *
     * @return string
     */
    public function getCodigoes()
    {
        return $this->codigoes;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Barrio
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Barrio
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set idcomuna
     *
     * @param string $idcomuna
     *
     * @return Barrio
     */
    public function setIdcomuna($idcomuna)
    {
        $this->idcomuna = $idcomuna;

        return $this;
    }

    /**
     * Get idcomuna
     *
     * @return string
     */
    public function getIdcomuna()
    {
        return $this->idcomuna;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="\saul\ComunBundle\Entity\Comuna", inversedBy="Barrio")
     * @ORM\JoinColumn(name="idcomuna", referencedColumnName="idcomuna")
     */
    private $comuna;
    
    public function setComuna(\saul\ComunBundle\Entity\Comuna $comuna)
    {
        $this->comuna  = $comuna;
        $this->idcomuna = $comuna->getIdcomuna();
    }
    
    public function getComuna()
    {
        return $this->comuna;
    }
}

