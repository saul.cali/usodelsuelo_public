<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Barrio
 *
 * @ORM\Table(name="tercero")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\TerceroRepository")
 */
class Tercero
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identificacion", type="string", length=20)
     */
    private $identificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idtipoidentificacion", type="integer")
     */
    private $idTipoIdentificacion;
    

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="razonsocial", type="string", length=100)
     */
    private $razonSocial;

   
	/**
     * @var integer
     *
     * @ORM\Column(name="idciudadnacimiento", type="integer")
     */
    private $idCiudadNacimiento;
	
	/**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;
	
	/**
     * @var string
     *
     * @ORM\Column(name="informacionadicional", type="string", length=255)
     */
    private $informacionAdicional;
	
    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=100)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;
        
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identificacion
     *
     * @param string $identificacion
     * @return Tercero
     */
    public function setIdentificacion($identificacion)
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    /**
     * Get identificacion
     *
     * @return string 
     */
    public function getIdentificacion()
    {
        return $this->identificacion;
    }

    /**
     * Set tipoIdentificacionId
     *
     * @param integer $idTipoIdentificacion
     * @return Tercero
     */
    public function setIdTipoIdentificacion($idTipoIdentificacion)
    {
        $this->idTipoIdentificacion = $idTipoIdentificacion;

        return $this;
    }

    /**
     * Get $idTipoIdentificacion
     *
     * @return integer 
     */
    public function getIdTipoIdentificacion()
    {
        return $this->idTipoIdentificacion;
    }
    
    
    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tercero
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Tercero
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Tercero
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }
        
    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Tercero
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }
	
	/**
     * Set informacionAdicional
     *
     * @param string $informacionAdicional
     * @return Tercero
     */
    public function setInformacionAdicional($informacionAdicional)
    {
        $this->informacionAdicional = $informacionAdicional;

        return $this;
    }

    /**
     * Get informacionAdicional
     *
     * @return string 
     */
    public function getInformacionAdicional()
    {
        return $this->informacionAdicional;
    }
	

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Tercero
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Tercero
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set idCiudadNacimiento
     *
     * @param integer $idCiudadNacimiento
     * @return Tercero
     */
    public function setIdCiudadNacimientoId($idCiudadNacimiento)
    {
        $this->idCiudadNacimiento = $idCiudadNacimiento;

        return $this;
    }

    /**
     * Get $idCiudadNacimiento
     *
     * @return integer 
     */
    public function getIdCiudadNacimiento()
    {
        return $this->idCiudadNacimiento;
    }    
	
	/**
     * @ORM\ManyToOne(targetEntity="\saul\ComunBundle\Entity\Ciudad")
     * @ORM\JoinColumn(name="idciudadnacimiento", referencedColumnName="idciudad")
     */
    private $ciudadNacimiento;
    
    public function setCiudadNacimiento(\saul\ComunBundle\Entity\Ciudad $ciudadNacimiento)
    {
        $this->ciudadNacimiento  = $ciudadNacimiento;
        $this->idCiudadNacimiento = $ciudadNacimiento->getIdCiudad();
    }
    
    public function getCiudadNacimiento()
    {
        return $this->ciudadNacimiento;
    }
	
	/**
     * @ORM\ManyToOne(targetEntity="\saul\ComunBundle\Entity\Ciudad")
     * @ORM\JoinColumn(name="idciudadresidencia", referencedColumnName="idciudad")
     */
    private $ciudadResidencia;
    
    public function setCiudadResidencia(\saul\ComunBundle\Entity\Ciudad $ciudadResidencia)
    {
        $this->ciudadResidencia  = $ciudadResidencia;
        $this->idCiudadResidencia = $ciudadResidencia->getIdCiudad();
    }
    
    public function getCiudadResidencia()
    {
        return $this->ciudadResidencia;
    }
	
	/**
     * @ORM\ManyToOne(targetEntity="\saul\ComunBundle\Entity\TipoDocIdentificacion")
     * @ORM\JoinColumn(name="idtipoidentificacion", referencedColumnName="idtipodocidentificacion")
     */
    private $tipoIdentificacion;
    
    public function setTipoIdentificacion(\saul\ComunBundle\Entity\TipoDocIdentificacion $tipoIdentificacion)
    {
        $this->tipoIdentificacion  = $tipoIdentificacion;
        $this->idTipoIdentificacion = $tipoIdentificacion->getIdTipoDocIdentificacion();
    }
    
    public function getTipoIdentificacion()
    {
        return $this->tipoIdentificacion;
    }
		
}
