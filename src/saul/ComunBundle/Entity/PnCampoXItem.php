<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCampo
 *
 * @ORM\Table(name="pncampoxitem")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\PnCampoXItemRepository")
 */
class PnCampoXItem
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="iditem", type="integer")
     * @ORM\Id
     */
    private $idItem;

    /**
     * @var int
     *
     * @ORM\Column(name="idcampo", type="string", length=255)
     * @ORM\Id
     */
    private $idCampo;
      
  
    /**
     * @var date
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var date
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    /**
     * Set idItem
     *
     * @param int $idItem
     *
     * @return PnItem
     */
    public function setIdItem($idItem)
    {
        $this->idItem = $idItem;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getIdCampo()
    {
        return $this->idCampo;
    }

    /**
     * Set idCampo
     *
     * @param int $idCampo
     *
     * @return PnCampo
     */
    public function setIdCampo($idCampo)
    {
        $this->idCampo = $idCampo;

        return $this;
    }

   
  
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Barrio
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Barrio
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}

