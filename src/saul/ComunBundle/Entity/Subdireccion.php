<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subdireccion
 *
 * @ORM\Table(name="subdireccion")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\SubdireccionRepository")
 */
class Subdireccion
{
    /**
     * @var int
     *
     * @ORM\Column(name="idsubdireccion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idsubdireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=45)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuario", type="bigint", nullable=true)
     */
    private $idusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="idradicadororfeo", type="string", length=5, nullable=true)
     */
    private $idradicadororfeo;

    /**
     * @var string
     *
     * @ORM\Column(name="dependenciaorfeo", type="string", length=5, nullable=true)
     */
    private $dependenciaorfeo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getIdsubdireccion()
    {
        return $this->idsubdireccion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Subdireccion
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Subdireccion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idusuario
     *
     * @param integer $idusuario
     *
     * @return Subdireccion
     */
    public function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return int
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }

    /**
     * Set idradicadororfeo
     *
     * @param string $idradicadororfeo
     *
     * @return Subdireccion
     */
    public function setIdradicadororfeo($idradicadororfeo)
    {
        $this->idradicadororfeo = $idradicadororfeo;

        return $this;
    }

    /**
     * Get idradicadororfeo
     *
     * @return string
     */
    public function getIdradicadororfeo()
    {
        return $this->idradicadororfeo;
    }

    /**
     * Set dependenciaorfeo
     *
     * @param string $dependenciaorfeo
     *
     * @return Subdireccion
     */
    public function setDependenciaorfeo($dependenciaorfeo)
    {
        $this->dependenciaorfeo = $dependenciaorfeo;

        return $this;
    }

    /**
     * Get dependenciaorfeo
     *
     * @return string
     */
    public function getDependenciaorfeo()
    {
        return $this->dependenciaorfeo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Subdireccion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Subdireccion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
     /**
     * @ORM\ManyToOne(targetEntity="\saul\SfGuardBundle\Entity\sf_guard_user", inversedBy="Subdireccion")
     * @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     * @return integer
     */
    private $subdirector;
    
    public function setSubdirector(\saul\SfGuardBundle\Entity\sf_guard_user $subdirector)
    {
        $this->subdirector  = $subdirector;
        $this->subdirector = $subdirector->getId();
    }
    
    public function getSubdirector()
    {
        return $this->subdirector;
    }
}

