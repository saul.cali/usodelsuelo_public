<?php

namespace saul\ComunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCampo
 *
 * @ORM\Table(name="tipocampo")
 * @ORM\Entity(repositoryClass="saul\ComunBundle\Repository\TipoCampoRepository")
 */
class TipoCampo
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="idtipocampo", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idTipoCampo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
   

    const CADENA = 'texto corto';
    const TEXTO = 'texto corto';
    const ENTERO = 'numero';
    const FECHA = 'fecha';

    /**
     * Get id
     *
     * @return int
     */
    public function getIdTipoCampo()
    {
        return $this->idTipoCampo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoCampo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
  
}

