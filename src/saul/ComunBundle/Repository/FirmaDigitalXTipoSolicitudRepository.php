<?php

namespace saul\ComunBundle\Repository;

/**
 * PruebaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FirmaDigitalXTipoSolicitudRepository extends \Doctrine\ORM\EntityRepository
{
    
    public function consultar($em, $idUsuario, $idTipoSolicitud) 
    {   
        $dql = "SELECT fdxts.idTipoSolicitud, fd.password, fd.createdAt, fd.sfGuardUserId
                FROM ComunBundle:FirmaDigitalXTipoSolicitud fdxts  
                LEFT JOIN ComunBundle:FirmaDigital fd WITH fd.idFirma=fdxts.idFirma
                WHERE fdxts.idTipoSolicitud=$idTipoSolicitud";
        $query = $em->createQuery($dql);
        return $query->getOneOrNullResult();
    }
}
