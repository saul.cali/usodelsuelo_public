<?php

namespace saul\UsoSueloBundle\Classes;

use saul\ComunBundle\Classes\ImpresionPDF;

/**
 * PlantillaRespuestaActividadesPermitidas
 *
 * Información de la Plantilla con la cual se genera el archivo PDF medicante la
 * clase CertificadoPDF
 *
 * @package    UsoSueloBundle
 * @author     Wilson Mendéz <wilson.mendez@cali.gov.co>
 * @version    0.1
 */
class PlantillaRespuestaActividadesPermitidas extends ImpresionPDF {

    /**
     *
     * @var Solicitud $objSolicitud Este objeto contiene la información de la
     * solicitud con la cual se llena el formulario.
     */
    public $objSolicitud = null;

    /**
     *
     * @var Array $arrayInfraccionXSolicitud Este es un array de objetos de tipo
     * InfraccionXSolicitud que estan relacionadas con la solicitud.
     */
    public $arrayInfraccionXSolicitud = null;

    /**
     *
     * @var Array $arraySolicitudxdocumento Este es un array de objetos de tipo
     * Solicitudxdocumento que estan relacionados con la solicitud. Para esta
     * plantilla hace referencia a las imagenes que anexa el Solicitante.
     */
    public $arraySolicitudxdocumento = null;

    /**
     *
     * Constructor del Objeto
     *
     * @param type $orientation
     * @param type $unit
     * @param type $format
     * @param type $unicode
     * @param type $encoding
     * @param type $diskcache
     * @param type $pdfa
     * @param type $container
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $container) {
	setlocale(LC_ALL, "es_CO");
	parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa, $container);
    }

    /**
     *
     * Genera el encabezado del documento.
     *
     * @access public
     */
    public function Header() {
	//$this->insertarMarcadeagua('marca.jpg');

	if ($this->page > 1) {
	    $this->SetFont('helvetica', 'A', 9);
	    $encabezado = '<table width="920px" style="text-align: justify" cellpadding="2" border="1">
	    <tr>
	      <th width="10%" align="center"><b>COD.CIIU</b></th>
	      <th width="30%" align="center"><b>NOMBRE ACTIVIDAD</b></th>
	      <th width="60%" align="center"><b>RESPUESTA</b></th>
	    </tr></table>';
	    $this->writeHTMLCell($w = 0, $h = 0, $x = 17, $y = 22, $encabezado, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
	}
    }

    public function Footer() {
	$this->SetAutoPageBreak(TRUE, 31);
	$this->SetY(-18);
	$this->SetFont('helvetica', 'I', 7.7);

	if ($this->page > 1) {
	    $this->SetY(-23);
	    $this->SetFont('helvetica', 'I', 8);
	}

	$textoCalidad = "Puede verificar su consulta posteriormente con el código: " . $this->codigo . ", accediendo al siguiente enlace. https://usodelsuelo.cali.gov.co/informacion#!/usosuelo/consultarcodigo/ \n";
	$textoPOT = 'El Concepto uso de suelo es un documento informativo y no autoriza el funcionamiento o apertura de establecimientos comerciales.
Para garantizar el adecuado desarrollo de la actividad económica se deberá adquirir la correspondiente licencia urbanística, que autorice la materialización del uso de suelo, por parte de los curadores urbanos.
La expedición de estos documentos informativos no otorga derechos ni obligaciones a su peticionario y no modifica los derechos conferidos mediante licencias urbanísticas que estén vigentes o que hayan sido ejecutadas.
El presente documento se expide con fundamento en lo establecido por el artículo 12 del Decreto 1203 de 2017, y las normas urbanísticas adoptadas por el Acuerdo 0373 de 2014';
	$this->MultiCell(0, 0, $textoPOT . "\n" . $textoCalidad . "\n", 0, 'C', 0, '', $this->GetX(), $this->GetY() - 6, true, 0, false, true, false, '', true);
	//$this->Cell(0, 10,  $textoCalidad . "\n" . $texto . "\n", 0, false, 'C', 0, '', 0, false, 'T', 'M');
	// Position at 15 mm from bottom
	$this->SetY(-10);
	// Set font
	//$this->SetFont('helvetica', 'I', 8);
	// Page number
	$this->Cell(0, 12, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    /**
     *
     * Función que contiene el cuerpo de la plantilla y genera el documento pdf.
     *
     * @access public
     */
    public function obtenerPlantilla() {

	$hoy = date("Y-m-d H:i:s");
	$data = $this->datos;
	//print_r($data);
	//die();
	$datos = null;
	foreach ($data['data'] as $value) {
	    $datos .= '<tr nobr="true">
			<td align="center">' . $value['codCiiu'] . '</td>
			<td>' . $value['nombreCiiu'] . '</td>
			<td>' . $value['respuesta'] . '</td>
		      </tr>';
	}


	$this->SetFont('helvetica', 'A', 9);
	$html = '<table>';
	$html .= '  <tr>';
	$html .= '      <td width="100px">';
	$html .= '          <img src="' . $this->dirImagenes . $this->logo . '" width="150">';
	$html .= '      </td>';
	$html .= '  </tr>';
	$html .= '</table>';
	$html .= '<br><br>';
	$html .= '<table width="920px" style="text-align: justify" cellpadding="3" border="0">
		    <tr>
		    <th width="60%" align="left">
		      <b>Dirección: </b>' . $this->direccion . '<br>
		      <b>Número Predial Nacional: </b>' . $this->npn . '<br>
		      <b>Área de Consulta: </b>' . $data['data'][0]['areaActividad'] . '<br>
		      <b>Tipo de Consulta: </b>' . $data['data'][0]['tipo'] . '<br>
		      <b>Vocación de Consulta: </b>' . $data['data'][0]['vocacion'] . '<br>
		    </th>
		      <th width="40%" align="left">
		      <b>Código de Consulta: </b>' . $this->codigo . '<br>
		      <b>Fecha: </b>' . $hoy . '<br><br></th></tr></table>';

	$html .= '<table width="920px" style="text-align: justify" cellpadding="2" border="1">
		    <tr>
		      <th width="10%" align="center"><b>COD.CIIU</b></th>
		      <th width="30%" align="center"><b>NOMBRE ACTIVIDAD</b></th>
		      <th width="60%" align="center"><b>RESPUESTA</b></th>
		    </tr>';
	$html .= $datos;
	$html .= '</table>';

	$style = array(
	    'border' => 0,
	    'vpadding' => 'auto',
	    'hpadding' => 'auto',
	    'fgcolor' => array(0, 0, 0),
	    'bgcolor' => false, //array(255,255,255)
	    'module_width' => 1, // width of a single module in points
	    'module_height' => 1 // height of a single module in points
	);

	$this->writeHTMLCell($w = 170, $h = 0, $x = '', $y = 10, $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

	/* $tbl = <<<EOD
	  <table border="1" cellpadding="2" cellspacing="0" style="text-align: justify">
	  <tr>
	  <th width="10%"><b>COD.CIIU</b></th>
	  <th width="30%"><b>NOMBRE ACTIVIDAD</b></th>
	  <th width="15%"><b>CONVENCION</b></th>
	  <th width="45%"><b>RESPUESTA</b></th>
	  </tr>
	  $datos
	  </table>
	  EOD;

	  $this->writeHTML($tbl, true, false, false, false, ''); */

	$this->Output(WEB_DIRECTORY . DIRECTORY_SEPARATOR . $this->archivo, 'I');
    }

    function f_remove_odd_characters($string) {
	// these odd characters appears usually 
	// when someone copy&paste from MSword to an HTML form
	$string = str_replace("\n", "[NEWLINE]", $string);
	$string = htmlentities($string);
	$string = preg_replace('/[^(\x20-\x7F)]*/', '', $string);
	$string = html_entity_decode($string);
	$string = str_replace("[NEWLINE]", "\n", $string);
	return $string;
    }

}

?>
