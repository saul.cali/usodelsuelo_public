<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Convencion
 *
 * @ORM\Table(name="pot2_convencion")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2ConvencionRepository")
 */
class Pot2Convencion {

    /**
     * @var int
     *
     * @ORM\Column(name="idconvencion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idconvencion;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=6)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=2000)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    

    /**
     * @var string
     *
     * @ORM\Column(name="mostrar", type="string", length=50)
     */
    private $mostrar;    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdconvencion() {
	return $this->idconvencion;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo() {
	return $this->codigo;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
	return $this->nombre;
    }
    
    /**
     * Get mostrar
     *
     * @return string
     */
    public function getMostrar() {
	return $this->mostrar;
    }    

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
	return $this->descripcion;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pot2Convencion
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pot2Convencion
     */
    public function setUpdatedAt($updatedAt) {
	$this->updatedAt = $updatedAt;

	return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
	return $this->updatedAt;
    }

}
