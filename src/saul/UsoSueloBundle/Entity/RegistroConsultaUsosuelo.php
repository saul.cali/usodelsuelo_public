<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Respuesta
 *
 * @ORM\Table(name="registroconsultausosuelo")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2RespuestaRepository")
 */
class RegistroConsultaUsosuelo {

    /**
     * @var int
     *
     * @ORM\Column(name="idregistroconsultausosuelo", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idregistroconsultausosuelo;

    /**
     * @var int
     * @ORM\Column(name="idareaactividad", type="integer", nullable=false)
     */
    private $idareaactividad;

    /**
     * Set idareaactividad
     *
     * @param int $idareaactividad
     */
    public function setIdareaactividad($idareaactividad) {
	$this->idareaactividad = $idareaactividad;
	return $this;
    }

    /**
     * @var int
     * @ORM\Column(name="idtipo", type="integer", nullable=false)
     */
    private $idtipo;

    /**
     * Set idtipo
     *
     * @param int $idtipo
     */
    public function setIdtipo($idtipo) {
	$this->idtipo = $idtipo;
	return $this;
    }

    /**
     * @var int
     * @ORM\Column(name="idvocacion", type="integer", nullable=false)
     */
    private $idvocacion;

    /**
     * Set idvocacion
     *
     * @param int $idvocacion
     */
    public function setIdvocacion($idvocacion) {
	$this->idvocacion = $idvocacion;
	return $this;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="idcomuna", type="integer", length=11)
     */
    private $idcomuna;

    /**
     * Set idcomuna
     *
     * @param int $idcomuna
     */
    public function setIdcomuna($idcomuna) {
	$this->idcomuna = $idcomuna;
	return $this;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="idbarrio", type="integer", length=11)
     */
    private $idbarrio;

    /**
     * Set idbarrio
     *
     * @param int $idbarrio
     */
    public function setIdbarrio($idbarrio) {
	$this->idbarrio = $idbarrio;
	return $this;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="codciiu", type="integer", length=50)
     */
    private $codciiu;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=250)
     */
    private $respuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="useragent", type="string", length=250)
     */
    private $useragent;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=50)
     */
    private $ip;

    /**
     * Set ip
     *
     * @param string pi
     */
    public function setIp($ip) {
	$this->ip = $ip;

	return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=150)
     */
    private $direccion;

    /**
     * 
     * @param type $direccion
     * @return $this
     */
    public function setDireccion($direccion) {
	$this->direccion = $direccion;

	return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="versionmapa", type="string", length=50)
     */
    private $versionmapa;

    /**
     * Set versionmapa
     *
     * @param string versionmapa
     */
    public function setVersionmapa($versionmapa) {
	$this->versionmapa = $versionmapa;

	return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=20)
     */
    private $codigo;

    /**
     * Set codigo
     *
     * @param string codigo
     */
    public function setCodigo($codigo) {
	$this->codigo = $codigo;

	return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="npn", type="string", length=30)
     */
    private $npn;

    /**
     * 
     * @param type $npn
     * @return $this
     */
    public function setNpn($npn) {
	$this->npn = $npn;

	return $this;
    }

    /**
     * Set respuesta
     *
     * @param string respuesta
     */
    public function setRespuesta($respuesta) {
	$this->respuesta = $respuesta;

	return $this;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Set codciiu
     *
     * @param string codciiu
     */
    public function setCodciiu($codciiu) {
	$this->codciiu = $codciiu;

	return $this;
    }

    /**
     * Get codciiu
     *
     * @return int
     */
    public function getCodciiu() {
	return $this->codciiu;
    }

    /**
     * Get Direccion
     *
     * @return string $direccion
     */
    public function getDireccion() {
	return $this->direccion;
    }

    /**
     * Get Npn
     *
     * @return int $npn
     */
    public function getNpn() {
	return $this->npn;
    }

    /**
     * Set useragent
     *
     * @param string $useragent
     */
    public function setUseragent($useragent) {
	$this->useragent = $useragent;

	return $this;
    }

    /**
     * Get useragent
     *
     * @return string
     */
    public function getUseragent() {
	return $this->useragent;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LineaDemarcacion
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2AreaActividad", inversedBy="RegistroConsultaUsosuelo")
     * @ORM\JoinColumn(name="idareaactividad", referencedColumnName="idareaactividad")
     */
    private $pot2areaactividad;

    public function setPot2AreaActividad(\saul\UsoSueloBundle\Entity\Pot2AreaActividad $pot2areaactividad) {
	$this->pot2areaactividad = $pot2areaactividad;
	$this->idareaactividad = $pot2areaactividad->getIdareaactividad();
    }

    public function getPot2AreaActividad() {
	return $this->pot2areaactividad;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Tipo", inversedBy="RegistroConsultaUsosuelo")
     * @ORM\JoinColumn(name="idtipo", referencedColumnName="idtipo")
     */
    private $pot2tipo;

    public function setPot2Tipo(\saul\UsoSueloBundle\Entity\Pot2Tipo $pot2tipo) {
	$this->pot2tipo = $pot2tipo;
	$this->idtipo = $pot2tipo->getIdtipo();
    }

    public function getPot2Tipo() {
	return $this->pot2tipo;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Vocacion", inversedBy="RegistroConsultaUsosuelo")
     * @ORM\JoinColumn(name="idvocacion", referencedColumnName="idvocacion")
     */
    private $pot2vocacion;

    public function setPot2Vocacion(\saul\UsoSueloBundle\Entity\Pot2Vocacion $pot2vocacion) {
	$this->pot2vocacion = $pot2vocacion;
	$this->idvocacion = $pot2vocacion->getIdvocacion();
    }

    public function getPot2Vocacion() {
	return $this->pot2vocacion;
    }

}
