<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2AreaxTipoxVocacion
 *
 * @ORM\Table(name="pot2_areaxtipoxvocacion")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2AreaxTipoxVocacionRepository")
 */
class Pot2AreaxTipoxVocacion {

    /**
     * @var int
     *
     * @ORM\Column(name="idareaxtipoxvocacion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idareaxtipoxvocacion;

    /**
     * @var int
     * @ORM\Column(name="idareaactividad", type="integer", nullable=false)
     */
    private $idareaactividad;

    /**
     * @var int
     * @ORM\Column(name="idtipo", type="integer", nullable=false)
     */
    private $idtipo;

    /**
     * @var int
     * @ORM\Column(name="idvocacion", type="integer", nullable=false)
     */
    private $idvocacion;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdAreaxTipoxVocacion() {
	return $this->idareaxtipoxvocacion;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2AreaActividad", inversedBy="Pot2AreaxTipoxVocacion")
     * @ORM\JoinColumn(name="idareaactividad", referencedColumnName="idareaactividad")
     */
    private $pot2areaactividad;

    public function setPot2AreaActividad($pot2areaactividad) {
		$this->pot2areaactividad = $pot2areaactividad;
		$this->idareaactividad = $pot2areaactividad->getIdareaactividad();
    }

    public function getPot2AreaActividad() {
	return $this->pot2areaactividad;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Tipo", inversedBy="Pot2AreaxTipoxVocacion")
     * @ORM\JoinColumn(name="idtipo", referencedColumnName="idtipo")
     */
    private $pot2tipo;

    public function setPot2Tipo(\saul\UsoSueloBundle\Entity\Pot2Tipo $pot2tipo) {
	$this->pot2tipo = $pot2tipo;
	$this->idtipo = $pot2tipo->getIdtipo();
    }

    public function getPot2Tipo() {
	return $this->pot2tipo;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Vocacion", inversedBy="Pot2AreaxTipoxVocacion")
     * @ORM\JoinColumn(name="idvocacion", referencedColumnName="idvocacion")
     */
    private $pot2vocacion;

    public function setPot2Vocacion(\saul\UsoSueloBundle\Entity\Pot2Vocacion $pot2vocacion) {
	$this->pot2vocacion = $pot2vocacion;
	$this->idvocacion = $pot2vocacion->getIdvocacion();
    }

    public function getPot2Vocacion() {
	return $this->pot2vocacion;
    }

}
