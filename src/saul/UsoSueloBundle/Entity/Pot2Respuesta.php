<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Respuesta
 *
 * @ORM\Table(name="pot2_respuesta")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2RespuestaRepository")
 */
class Pot2Respuesta {

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="idareaxtipoxvocacion", type="integer", nullable=false)
     */
    private $idareaxtipoxvocacion;
    
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="idciiu", type="integer", nullable=false)
     */
    private $idciiu;    

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="idconvencion", type="integer", nullable=false)
     */
    private $idconvencion;       

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="idcondicion", type="integer", nullable=false)
     */
    private $idcondicion;     

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2AreaxTipoxVocacion", inversedBy="Pot2Respuesta")
     * @ORM\JoinColumn(name="idareaxtipoxvocacion", referencedColumnName="idareaxtipoxvocacion")
     */
    private $pot2areaxtipoxvocacion;

    public function setPot2AreaxTipoxVocacion(\saul\UsoSueloBundle\Entity\Pot2AreaActividad $Pot2AreaxTipoxVocacion) {
	$this->pot2areaxtipoxvocacion = $Pot2AreaxTipoxVocacion;
	$this->idareaxtipoxvocacion = $Pot2AreaxTipoxVocacion->getIdareaactividad();
    }

    public function getPot2AreaxTipoxVocacion() {
	return $this->pot2areaxtipoxvocacion;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Ciiu", inversedBy="Pot2Respuesta")
     * @ORM\JoinColumn(name="idciiu", referencedColumnName="idciiu")
     */
    private $pot2ciiu;

    public function setPot2Ciiu(\saul\UsoSueloBundle\Entity\Pot2Ciiu $pot2ciiu) {
	$this->pot2ciiu = $pot2ciiu;
	$this->idciiu = $pot2ciiu->getIdciiu();
    }

    public function getPot2Ciiu() {
	return $this->pot2ciiu;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Convencion", inversedBy="Pot2Respuesta")
     * @ORM\JoinColumn(name="idconvencion", referencedColumnName="idconvencion")
     */
    private $pot2convencion;

    public function setPot2Convencion(\saul\UsoSueloBundle\Entity\Pot2Convencion $pot2convencion) {
	$this->pot2convencion = $pot2convencion;
	$this->idconvencion = $pot2convencion->getIdcovencion();
    }

    public function getPot2Convencion() {
	return $this->pot2convencion;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\saul\UsoSueloBundle\Entity\Pot2Condicion", inversedBy="Pot2Respuesta")
     * @ORM\JoinColumn(name="idcondicion", referencedColumnName="idcondicion")
     */
    private $pot2condicion;

    public function setPot2Condicion(\saul\UsoSueloBundle\Entity\Pot2Condicion $pot2condicion) {
	$this->pot2condicion = $pot2condicion;
	$this->idcondicion = $pot2condicion->getIdcondicion();
    }

    public function getPot2Condicion() {
	return $this->pot2condicion;
    }

}