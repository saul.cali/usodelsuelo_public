<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Condicion
 *
 * @ORM\Table(name="pot2_condicion")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2CondicionRepository")
 */
class Pot2Condicion {

    /**
     * @var int
     *
     * @ORM\Column(name="idcondicion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idcondicion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=2000)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="eir", type="integer", length=2)
     */
    private $eir;
 
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdcondicion() {
	return $this->idcondicion;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
	return $this->descripcion;
    }

    /**
     * Get eir
     *
     * @return string
     */
    public function getEir() {
	return $this->eir;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pot2Condicion
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pot2Condicion
     */
    public function setUpdatedAt($updatedAt) {
	$this->updatedAt = $updatedAt;

	return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
	return $this->updatedAt;
    }

}
