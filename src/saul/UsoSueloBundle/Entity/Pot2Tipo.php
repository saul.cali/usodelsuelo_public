<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Tipo
 *
 * @ORM\Table(name="pot2_tipo")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2TipoRepository")
 */
class Pot2Tipo {

    /**
     * @var int
     *
     * @ORM\Column(name="idtipo", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idtipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdtipo() {
	return $this->idtipo;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
	return $this->nombre;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pot2Tipo
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pot2Tipo
     */
    public function setUpdatedAt($updatedAt) {
	$this->updatedAt = $updatedAt;

	return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
	return $this->updatedAt;
    }

}
