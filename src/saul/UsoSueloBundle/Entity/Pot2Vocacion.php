<?php

namespace saul\UsoSueloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pot2Vocacion
 *
 * @ORM\Table(name="pot2_vocacion")
 * @ORM\Entity(repositoryClass="saul\UsoSueloBundle\Repository\Pot2VocacionRepository")
 */
class Pot2Vocacion {

    /**
     * @var int
     *
     * @ORM\Column(name="idvocacion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idvocacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getIdvocacion() {
	return $this->idvocacion;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
	return $this->nombre;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pot2Vocacion
     */
    public function setCreatedAt($createdAt) {
	$this->createdAt = $createdAt;

	return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
	return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pot2Vocacion
     */
    public function setUpdatedAt($updatedAt) {
	$this->updatedAt = $updatedAt;

	return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
	return $this->updatedAt;
    }

}
