<?php

namespace saul\UsoSueloBundle\Repository;

/**
 * Pot2Respuesta
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Pot2RespuestaRepository extends \Doctrine\ORM\EntityRepository {

    function consultarPermitidos($usodelsueloservice, $currentPage, $numPerPage, $orderBy, $dataArray) {

	$em = $this->getEntityManager();
	$arrayColumns = [
	    'idareaactividad' => 'p2atv.idareaactividad',
	    'idtipo' => 'p2atv.idtipo',
	    'idvocacion' => 'p2atv.idvocacion',
	    'codCiiu' => ' p2cii.codigo',
	    'nombreCiiu' => ' p2cii.nombre'
	];

	$dqlSelect = "SELECT 
                        p2aa.nombre as areaActividad,
                        p2t.nombre as tipo,
                        p2v.nombre as vocacion,
                        concat(p2cii.codigo,' ') as codCiiu,
			p2cii.nombre as nombreCiiu,
			p2cii.palabra_clave as palabraClave,
			p2cv.mostrar as nombreConvencion, 
			p2cv.descripcion as observacion,
                        p2cd.descripcion as condicion ";
	$dqlSelectCount = "SELECT count(p2resp) ";
	$dqlFrom = "FROM UsoSueloBundle:Pot2Respuesta p2resp
                    LEFT JOIN UsoSueloBundle:Pot2AreaxTipoxVocacion p2atv WITH p2resp.idareaxtipoxvocacion = p2atv.idareaxtipoxvocacion
                    LEFT JOIN UsoSueloBundle:Pot2AreaActividad p2aa WITH p2aa.idareaactividad = p2atv.idareaactividad
		    LEFT JOIN UsoSueloBundle:Pot2Tipo p2t WITH p2t.idtipo = p2atv.idtipo
		    LEFT JOIN UsoSueloBundle:Pot2Vocacion p2v WITH p2v.idvocacion = p2atv.idvocacion
		    LEFT JOIN UsoSueloBundle:Pot2Ciiu p2cii WITH p2cii.idciiu = p2resp.idciiu
		    LEFT JOIN UsoSueloBundle:Pot2Convencion p2cv WITH p2cv.idconvencion = p2resp.idconvencion
		    LEFT JOIN UsoSueloBundle:Pot2Condicion p2cd WITH p2cd.idcondicion = p2resp.idcondicion
                    WHERE p2cv.nombre != 'No Permitido'";

	foreach ($dataArray as $key => $value) {
	    if (is_bool($value) || is_numeric($value)) {
		$dqlFrom .= " AND " . $arrayColumns[$key] . "='$value'";
	    } else {
		$dqlFrom .= " AND " . $arrayColumns[$key] . " LIKE '%$value%'";
	    }
	}
	//#cantidad de registros
	$queryCount = $em->createQuery($dqlSelectCount . $dqlFrom . ' GROUP BY p2cii.codigo');
	//$dqlFrom .= " GROUP BY p2aa.nombre,p2t.nombre,p2v.nombre,p2cii.codigo,p2cii.nombre,p2cii.palabra_clave ORDER BY " . $arrayColumns[$orderBy] . " ASC";
	$dqlFrom .= " ORDER BY" . $arrayColumns[$orderBy] . " ASC";

	$query = $em->createQuery($dqlSelect . $dqlFrom);

	$result = $query->getResult();
	//$resultCount = $queryCount->getResult();
	///aqui llamo el servicio donde esta toda la logica para leer las respuestas
	if ($result) {
	    $datos = $usodelsueloservice->buscarCodigoRepetido($result);
	    //$datosCount = $usodelsueloservice->buscarCodigoRepetido($resultCount);
	    $datosFiltrado = [];
	    for ($i = 0; $i < $numPerPage; $i++) {
		if (isset($datos[$numPerPage * ($currentPage - 1) + $i])) {
		    $datosFiltrado[] = $datos[$numPerPage * ($currentPage - 1) + $i];
		} else {
		    break;
		}
	    }
	    return ['data' => $datosFiltrado, 'totalItems' => count($datos)];
	} else {
	    return ['data' => "nopermite", 'totalItems' => 0];
	}
    }

    function consultarTodos($usodelsueloservice, $currentPage, $numPerPage, $orderBy, $dataArray) {

	$em = $this->getEntityManager();
	$arrayColumns = [
	    'idareaactividad' => 'p2atv.idareaactividad',
	    'idtipo' => 'p2atv.idtipo',
	    'idvocacion' => 'p2atv.idvocacion',
	    'codCiiu' => ' p2cii.codigo',
	    'nombreCiiu' => ' p2cii.nombre'
	];

	$dqlSelect = "SELECT 
			p2aa.nombre as areaActividad,
                        p2t.nombre as tipo,
                        p2v.nombre as vocacion,
                        concat(p2cii.codigo,' ') as codCiiu,
			p2cii.nombre as nombreCiiu,
			p2cii.palabra_clave as palabraClave,
			p2cv.mostrar as nombreConvencion, 
			p2cv.descripcion as observacion,
                        p2cd.descripcion as condicion ";
	$dqlSelectCount = "SELECT count(p2resp) ";
	$dqlFrom = "FROM UsoSueloBundle:Pot2Respuesta p2resp
                    LEFT JOIN UsoSueloBundle:Pot2AreaxTipoxVocacion p2atv WITH p2resp.idareaxtipoxvocacion = p2atv.idareaxtipoxvocacion
                    LEFT JOIN UsoSueloBundle:Pot2AreaActividad p2aa WITH p2aa.idareaactividad = p2atv.idareaactividad
		    LEFT JOIN UsoSueloBundle:Pot2Tipo p2t WITH p2t.idtipo = p2atv.idtipo
		    LEFT JOIN UsoSueloBundle:Pot2Vocacion p2v WITH p2v.idvocacion = p2atv.idvocacion
		    LEFT JOIN UsoSueloBundle:Pot2Ciiu p2cii WITH p2cii.idciiu = p2resp.idciiu
		    LEFT JOIN UsoSueloBundle:Pot2Convencion p2cv WITH p2cv.idconvencion = p2resp.idconvencion
		    LEFT JOIN UsoSueloBundle:Pot2Condicion p2cd WITH p2cd.idcondicion = p2resp.idcondicion
                    WHERE 1 = 1";

	foreach ($dataArray as $key => $value) {
	    if (is_bool($value) || is_numeric($value)) {
		$dqlFrom .= " AND " . $arrayColumns[$key] . "='$value'";
	    } else {
		$dqlFrom .= " AND " . $arrayColumns[$key] . " LIKE '%$value%'";
	    }
	}

	$dqlFrom .= " ORDER BY" . $arrayColumns[$orderBy] . " ASC";

	$query = $em->createQuery($dqlSelect . $dqlFrom)
		->setMaxResults($numPerPage)
		->setFirstResult($numPerPage * ($currentPage - 1));
	$queryCount = $em->createQuery($dqlSelectCount . $dqlFrom);
	$result = $query->getResult();
	//$resultCount = $queryCount->getSingleScalarResult();
	///aqui llamo el servicio donde esta toda la logica para leer las respuestas
	$datos = $usodelsueloservice->buscarCodigoRepetido($result);

	return ['data' => $datos];
    }

    function consultarRespuesta($usodelsueloservice, $dataArray) {
	$em = $this->getEntityManager();

	$arrayColumns = [
	    'idareaactividad' => 'p2atv.idareaactividad',
	    'idtipo' => 'p2atv.idtipo',
	    'idvocacion' => 'p2atv.idvocacion',
	    'idCiiu' => 'p2cii.idciiu'
	];

	//print_r($dataArray);
	//die();
	$dqlSelect = "SELECT 
			p2aa.nombre as areaActividad,
                        p2t.nombre as tipo,
                        p2v.nombre as vocacion,
                        concat(p2cii.codigo,' ') as codCiiu,
			p2cii.nombre as nombreCiiu,
			p2cii.palabra_clave as palabraClave,
			p2cv.mostrar as nombreConvencion, 
			p2cv.descripcion as observacion,
                        p2cd.descripcion as condicion ";
	$dqlSelectCount = "SELECT count(p2resp) ";
	$dqlFrom = "FROM UsoSueloBundle:Pot2Respuesta p2resp
                    LEFT JOIN UsoSueloBundle:Pot2AreaxTipoxVocacion p2atv WITH p2resp.idareaxtipoxvocacion = p2atv.idareaxtipoxvocacion
                    LEFT JOIN UsoSueloBundle:Pot2AreaActividad p2aa WITH p2aa.idareaactividad = p2atv.idareaactividad
		    LEFT JOIN UsoSueloBundle:Pot2Tipo p2t WITH p2t.idtipo = p2atv.idtipo
		    LEFT JOIN UsoSueloBundle:Pot2Vocacion p2v WITH p2v.idvocacion = p2atv.idvocacion
		    LEFT JOIN UsoSueloBundle:Pot2Ciiu p2cii WITH p2cii.idciiu = p2resp.idciiu
		    LEFT JOIN UsoSueloBundle:Pot2Convencion p2cv WITH p2cv.idconvencion = p2resp.idconvencion
		    LEFT JOIN UsoSueloBundle:Pot2Condicion p2cd WITH p2cd.idcondicion = p2resp.idcondicion
                    WHERE 1 = 1";

	foreach ($dataArray as $key => $value) {
	    if ($key == 'idCiiu') {
		$dqlFrom .= " AND " . 'p2cii.idciiu IN (';
		foreach ($value as $idciiu) {
		    $dqlFrom .= $idciiu . ',';
		}
		$dqlFrom .= '0 )';
	    } elseif (is_bool($value) || is_numeric($value)) {
		$dqlFrom .= " AND " . $arrayColumns[$key] . "='$value'";
	    } else {
		$dqlFrom .= " AND " . $arrayColumns[$key] . " LIKE '%$value%'";
	    }
	}

	$query = $em->createQuery($dqlSelect . $dqlFrom);
	$queryCount = $em->createQuery($dqlSelectCount . $dqlFrom);
	$result = $query->getResult();


	/* echo $query->getSQL();
	  die();
	  print_r($result); */
	$resultCount = $queryCount->getSingleScalarResult();

	///aqui llamo el servicio donde esta toda la logica para leer las respuestas
	if (count($result) > 0) {
	    $datos = $usodelsueloservice->buscarCodigoRepetido($result);
	    return ['data' => $datos];
	}
    }

    function consultarPermitidosImpresion($usodelsueloservice, $dataArray) {

	$em = $this->getEntityManager();
	$arrayColumns = [
	    'idareaactividad' => 'p2atv.idareaactividad',
	    'idtipo' => 'p2atv.idtipo',
	    'idvocacion' => 'p2atv.idvocacion'
	];

	$dqlSelect = "SELECT 
                        p2aa.nombre as areaActividad,
                        p2t.nombre as tipo,
                        p2v.nombre as vocacion,
                        concat(p2cii.codigo,' ') as codCiiu,
			p2cii.nombre as nombreCiiu,
			p2cii.palabra_clave as palabraClave,
			p2cv.mostrar as nombreConvencion, 
			p2cv.descripcion as observacion,
                        p2cd.descripcion as condicion ";
	$dqlSelectCount = "SELECT count(p2resp) ";
	$dqlFrom = "FROM UsoSueloBundle:Pot2Respuesta p2resp
                    LEFT JOIN UsoSueloBundle:Pot2AreaxTipoxVocacion p2atv WITH p2resp.idareaxtipoxvocacion = p2atv.idareaxtipoxvocacion
                    LEFT JOIN UsoSueloBundle:Pot2AreaActividad p2aa WITH p2aa.idareaactividad = p2atv.idareaactividad
		    LEFT JOIN UsoSueloBundle:Pot2Tipo p2t WITH p2t.idtipo = p2atv.idtipo
		    LEFT JOIN UsoSueloBundle:Pot2Vocacion p2v WITH p2v.idvocacion = p2atv.idvocacion
		    LEFT JOIN UsoSueloBundle:Pot2Ciiu p2cii WITH p2cii.idciiu = p2resp.idciiu
		    LEFT JOIN UsoSueloBundle:Pot2Convencion p2cv WITH p2cv.idconvencion = p2resp.idconvencion
		    LEFT JOIN UsoSueloBundle:Pot2Condicion p2cd WITH p2cd.idcondicion = p2resp.idcondicion
                    WHERE p2cv.nombre != 'No Permitido'";

	foreach ($dataArray as $key => $value) {
	    if (is_bool($value) || is_numeric($value)) {
		$dqlFrom .= " AND " . $arrayColumns[$key] . "='$value'";
	    } else {
		$dqlFrom .= " AND " . $arrayColumns[$key] . " LIKE '%$value%'";
	    }
	}
	//#cantidad de registros
	$queryCount = $em->createQuery($dqlSelectCount . $dqlFrom . ' GROUP BY p2cii.codigo');

	$query = $em->createQuery($dqlSelect . $dqlFrom);

	$result = $query->getResult();

	///aqui llamo el servicio donde esta toda la logica para leer las respuestas
	$datos = $usodelsueloservice->buscarCodigoRepetido($result);

	return ['data' => $datos, 'totalItems' => count($datos)];
    }

}
