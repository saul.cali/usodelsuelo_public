<?php

namespace saul\UsoSueloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\Query\ResultSetMapping;

class UsoDelSueloController extends Controller {
    
     /**
     * @Route("/buscarcodigorepetido/")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function buscarCodigoRepetido($result) {

	$buscarcodigo[] = "";
	$result2[] = "";

	$buscarcodigo = [];

	for ($i = 0; $i < count($result); $i++) {
	    if (isset($buscarcodigo[$result[$i]['codCiiu']])) {
		continue;
	    } else {
		$buscarcodigo[$result[$i]['codCiiu']] = [
		    'areaActividad' => $result[$i]['areaActividad'],
		    'tipo' => $result[$i]['tipo'],
		    'vocacion' => $result[$i]['vocacion'],
		    'codCiiu' => $result[$i]['codCiiu'],
		    'nombreCiiu' => $result[$i]['nombreCiiu'],
		    'palabraClave' => $result[$i]['palabraClave'],
		    'datos' => [[
		    'nombreConvencion' => $result[$i]['nombreConvencion'],
		    'observacion' => $result[$i]['observacion'],
		    'condicion' => $result[$i]['condicion']
			]]
		];
		for ($j = $i + 1; $j < count($result); $j++) {
		    if ($result[$i]['codCiiu'] == $result[$j]['codCiiu']) {
			array_push($buscarcodigo[$result[$j]['codCiiu']]['datos'], [
			    'nombreConvencion' => $result[$j]['nombreConvencion'],
			    'observacion' => $result[$j]['observacion'],
			    'condicion' => $result[$j]['condicion']
				]
			);
		    }
		}
	    }
	}

	///////////////////////////////////

	foreach ($buscarcodigo as $value) {
	    $nombreConvencion = "";
	    $respuesta = "";

	    if (count($value['datos']) == 1) {
		if ($value['datos'][0]['nombreConvencion'] != 'Permitido Condicionado') {
		    $respuesta = $value['datos'][0]['observacion'];
		    $nombreConvencion = $value['datos'][0]['nombreConvencion'];
		} else {
		    $respuesta = $value['datos'][0]['condicion'];
		    $nombreConvencion = "".$value['datos'][0]['nombreConvencion'];
		}
		//echo $value['codCiiu'] . 'if   --> ' . count($value['datos']) . '<br>';
		//continue;
	    } else {

		//echo $value['codCiiu'] . 'else --> ' . count($value['datos']) . '<br>';

		$C1 = "";
		$puntos = 0;
		$observaciones = "";
		
		foreach ($value['datos'] as $key => $dato) {
		    //logica de respuesta
		    if ($dato['nombreConvencion'] == 'Permitido Condicionado') {
			$C1 .= $dato['condicion'];
		    } else {
			$observaciones .= $dato['observacion'] . '<br>';
		    }

		    //logica de nombre
		    if ($dato['nombreConvencion'] == 'Eir') {
			$nombreConvencion = $dato['nombreConvencion'];
			$puntos = 2;
		    } elseif ($puntos < 1 && $dato['nombreConvencion'] == 'Permitido Condicionado') {
			$nombreConvencion = $dato['nombreConvencion'];
			$puntos = 1;
		    } elseif ($puntos == 0) {
			$nombreConvencion = $dato['nombreConvencion'];
			$puntos = 0;
		    }
		}
		$respuesta = $C1 . '<br>' . $observaciones;
	    }


	    $data[] = [
		'areaActividad' => $value['areaActividad'],
		'tipo' => $value['tipo'],
		'vocacion' => $value['vocacion'],
		'codCiiu' => $value['codCiiu'],
		'nombreCiiu' => $value['nombreCiiu'],
		'palabraClave' => $value['palabraClave'],
		'nombreConvencion' => $nombreConvencion,
		'respuesta' => $respuesta
	    ];
	}

	return($data);

	/*echo '<pre>';
	print_r($data);
	echo '<pre>';*/
    }

}
