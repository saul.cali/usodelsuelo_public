<?php

namespace saul\UsoSueloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use saul\UsoSueloBundle\Entity\Pot2AreaxTipoxVocacion;
use saul\UsoSueloBundle\Entity\Pot2Respuesta;
use saul\UsoSueloBundle\Entity\RegistroConsultaUsosuelo;
use Doctrine\ORM\Query\ResultSetMapping;
use saul\UsoSueloBundle\Classes\PlantillaRespuestaActividadesPermitidas;

class ConsultaActividadesController extends Controller {

    /**
     * @Route("/consultarcodigo/{codigo}")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function consultarCodigo(Request $request) {

	$codigo = $request->get('codigo');

	$sql = "SELECT
	pot2_area_actividad.nombre AS AreaActividad,
	pot2_tipo.nombre AS Tipo,
	pot2_vocacion.nombre AS Vocacion,
	registroconsultausosuelo.direccion AS Direccion,
	registroconsultausosuelo.npn AS Npn,
	registroconsultausosuelo.idcomuna AS Comuna,
	barrio.nombre AS Barrio,
	pot2_ciiu.codigo AS Codciiu,
	pot2_ciiu.nombre AS Ciiu,
	registroconsultausosuelo.respuesta,
	registroconsultausosuelo.codigo as Codigoconsulta,
	registroconsultausosuelo.created_at as Fechaconsulta
	FROM
	registroconsultausosuelo
	JOIN pot2_ciiu ON registroconsultausosuelo.codciiu = pot2_ciiu.codigo
	LEFT JOIN barrio ON LPAD(registroconsultausosuelo.idbarrio,4,'0') = barrio.codigo
	JOIN pot2_area_actividad ON registroconsultausosuelo.idareaactividad = pot2_area_actividad.idareaactividad
	JOIN pot2_tipo ON registroconsultausosuelo.idtipo = pot2_tipo.idtipo
	JOIN pot2_vocacion ON registroconsultausosuelo.idvocacion = pot2_vocacion.idvocacion
	WHERE
	registroconsultausosuelo.codigo = '$codigo'";


	$em = $this->getDoctrine()->getEntityManager();
	$stmt = $em->getConnection()->prepare($sql);
	$stmt->execute();
	$registros = $stmt->fetchAll();

	$serializer = $this->get('serializer');
	if (count($registros) > 0) {
	    $response = ['data' => $registros, 'success' => true];
	} else {
	    $response = ['data' => null, 'success' => false];
	}

	return new Response($serializer->serialize($response, 'json'));
    }

    /**
     * @Route("/descargarmatriz")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function descargarMatrizAction() {
	$sql = "SELECT
	pot2_ciiu.idciiu,
	pot2_ciiu.codigo AS CodCiiu,
	pot2_ciiu.nombre AS NombreActividad,
	pot2_area_actividad.nombre AS AreaActividad,
	pot2_area_actividad.idareaactividad,
	pot2_tipo.nombre AS Tipo,
	pot2_tipo.idtipo,
	pot2_vocacion.nombre AS Vocacion,
	pot2_vocacion.idvocacion,
		GROUP_CONCAT(
		pot2_convencion.codigo SEPARATOR ' / '
	) AS respuesta,
	pot2_respuesta.idconvencion,
	pot2_respuesta.idcondicion,
	pot2_condicion.descripcion as Condicion
	FROM
		pot2_respuesta
	JOIN pot2_area_actividad ON pot2_respuesta.idareaactividad = pot2_area_actividad.idareaactividad
	JOIN pot2_tipo ON pot2_respuesta.idtipo = pot2_tipo.idtipo
	JOIN pot2_vocacion ON pot2_respuesta.idvocacion = pot2_vocacion.idvocacion
	JOIN pot2_convencion ON pot2_respuesta.idconvencion = pot2_convencion.idconvencion
	JOIN pot2_ciiu ON pot2_respuesta.idciiu = pot2_ciiu.idciiu
	LEFT JOIN pot2_condicion ON pot2_respuesta.idcondicion = pot2_condicion.idcondicion
	GROUP BY
	idvocacion, idtipo, idareaactividad, idciiu, idcondicion
	ORDER BY
	pot2_ciiu.codigo,
	pot2_respuesta.idareaactividad,
	pot2_respuesta.idtipo,
	pot2_respuesta.idvocacion";

	$em = $this->getDoctrine()->getEntityManager();
	$stmt = $em->getConnection()->prepare($sql);
	$stmt->execute();
	$registros = $stmt->fetchAll();

	//$i=2000;
	$html = '<table>';
	$tr = [];
	$trCondicion = [];
	$areasTiposVocaciones = [];

	foreach ($registros as $registro) {
	    if (!isset($tr[$registro['NombreActividad']])) {
		$tr[$registro['NombreActividad']] = $registro['CodCiiu'] . '|*' . $registro['NombreActividad'];
		$trCondicion[$registro['NombreActividad']] = $registro['Condicion'];
	    }
	    if (!isset($areasTiposVocaciones[$registro['AreaActividad'] . '|*' . $registro['Tipo'] . '|*' . $registro['Vocacion']])) {
		$areasTiposVocaciones[$registro['AreaActividad'] . '|*' . $registro['Tipo'] . '|*' . $registro['Vocacion']] = $registro['AreaActividad'] . '|*' . $registro['Tipo'] . '|*' . $registro['Vocacion'];
	    }
	    $tr[$registro['NombreActividad']] .= ('|*' . $registro['respuesta']);
	}

	//creamos la cabecera
	$html = '<table border="1px" cellspacing="0" cellpadding="2" style="font-size: 10px;font-family: Arial">';
	$trArea = '';
	$trTipo = '';
	$trVocacion = '';
	foreach ($areasTiposVocaciones as $header) {
	    list($trAreaTmp, $trTipoTmp, $trVocacionTmp) = explode('|*', $header);
	    $trArea .= '<td>' . $trAreaTmp . '</td>';
	    $trTipo .= '<td>' . $trTipoTmp . '</td>';
	    $trVocacion .= '<td>' . $trVocacionTmp . '</td>';
	}
	$html .= '<tr><td></td><td></td>' . $trArea . '<td></td></tr>';
	$html .= '<tr><td></td><td></td>' . $trTipo . '<td></td></tr>';
	$html .= '<tr><td>CodCiiu</td><td>Actividad</td>' . $trVocacion . '<td>Condición</td></tr>';

	foreach ($tr as $key => $reg) {
	    $html .= '<tr><td>' . str_replace('|*', '</td><td>', $reg) . '</td><td>' . $trCondicion[$key] . '</td></tr>';
	}

	$sqlConvencion = "SELECT pot2_convencion.codigo,
	pot2_convencion.descripcion,
	pot2_convencion.nombre FROM
	pot2_convencion";

	$em = $this->getDoctrine()->getEntityManager();
	$stmt = $em->getConnection()->prepare($sqlConvencion);
	$stmt->execute();
	$convenciones = $stmt->fetchAll();

	$trContenido = "";

	foreach ($convenciones as $convencion) {
	    $trContenido .= '<tr><td>' . $convencion['codigo'] . '</td>';
	    $trContenido .= '<td>' . $convencion['descripcion'] . '</td>';
	    $trContenido .= '<td>' . $convencion['nombre'] . '</td></tr>';
	}

	$convencion = '<table><p></p><br><div tyle="font-size: 16px;font-family: Arial"><b>TABLA DE CONVENCIONES</b></div><p></p></table><table border="1px" cellspacing="0" cellpadding="2" style="font-size: 10px;font-family: Arial">'
		. '<tr><td><b>Codigo</b></td><td><b>Descripcioón</b></td><td><b>Nombre</b></td></tr>'
		. $trContenido
		. '</table>';


	echo $html . $convencion;
	exit();
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function indexAction() {
	return $this->render('mapa.html.twig');
    }

    /**
     * @Route("/informacion")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function informacionAction() {
	return $this->render('template.html.twig');
    }

    /**
     * @Route("{idComuna}/{idBarrio}/{idAreaActividad}/{idTipo}/{idVocacion}/{npn}/{direccion}")
     * @Method({"GET"})
     * 
     * @param type $idComuna
     * @param type $idBarrio
     * @param type $idAreaActividad
     * @param type $idTipo
     * @param type $idVocacion
     * @return Response
     */
    public function consultar($idComuna, $idBarrio, $idAreaActividad, $idTipo, $idVocacion, $npn = null, $direccion = null) {

	$serializer = $this->get('serializer');
	$response = ['data' => null];

	if ($this->esPredioZonaValida($idAreaActividad, $idTipo, $idVocacion, $npn)) {
	    $em = $this->getDoctrine()->getManager();
	    $request = Request::createFromGlobals();

	    $codCiiu = $request->get('codCiiu');
	    $nomCiiu = $request->get('nombreCiiu');

	    if ($codCiiu != "") {
		$Ciiu = $codCiiu;
	    }
	    if ($nomCiiu != "") {
		$Ciiu = $nomCiiu;
	    }
	    if ($codCiiu != "" && $nomCiiu != "") {
		$Ciiu = $codCiiu . ' - ' . $nomCiiu;
	    }

	    $dataArray = array();

	    $currentPage = $request->get('currentPage');
	    $numPerPage = $request->get('numPerPage');
	    $orderBy = $request->get('orderBy');

	    $dataArray['idareaactividad'] = $idAreaActividad;
	    $dataArray['idtipo'] = $idTipo;
	    $dataArray['idvocacion'] = $idVocacion;
	    $dataArray['nombreCiiu'] = $nomCiiu;
	    $dataArray['codCiiu'] = $codCiiu;
	    $codBarrio = str_pad($idBarrio, 4, '0', STR_PAD_LEFT);

	    $objBarrioNombre = null;
	    $comuna = null;
	    $objCorregimientoNombre = null;
	    $objVeredaNombre = null;

	    if ($idComuna <= 23) {
		$objBarrio = $em->getRepository('ComunBundle:Barrio')->findOneBy(array('codigo' => $codBarrio));
		if (!$objBarrio) {
		    $objBarrioNombre = "Barrio No Encontrado";
		} else {
		    $objBarrioNombre = $objBarrio->getNombre();
		}
		$comuna = $idComuna;
	    }
	    if ($idComuna > 23) {
		$objCorregimiento = $em->getRepository('ComunBundle:Corregimiento')->findOneBy(array('codigo' => $idComuna));
		if (!$objCorregimiento) {
		    $objCorregimientoNombre = "Corregimiento No Encontrado";
		} else {
		    $objCorregimientoNombre = $objCorregimiento->getNombre();
		}

		$objVereda = $em->getRepository('ComunBundle:Vereda')->findOneBy(array('codigo' => $codBarrio));
		if (!$objVereda) {
		    $objVeredaNombre = "Vereda No Encontrada";
		} else {
		    $objVeredaNombre = $objVereda->getNombre();
		}
	    }

	    $codigo = mb_strtoupper($this->generarCodigo(10));
	    //69
	    $result = $em->getRepository('UsoSueloBundle:Pot2Respuesta')->consultarPermitidos($this->get('saul.usodelsuelo'), $currentPage, $numPerPage, $orderBy, $dataArray);
	    //600
	    $resultodos = $em->getRepository('UsoSueloBundle:Pot2Respuesta')->consultarTodos($this->get('saul.usodelsuelo'), $currentPage, $numPerPage, $orderBy, $dataArray);

	    if ($result) {
		$url = $this->consultarUrl($idAreaActividad, $idTipo, $idVocacion);
		$response = [
		    'data' => $result['data'],
		    'totalItems' => $result['totalItems'],
		    'alldata' => $resultodos['data'],
		    'direccion' => $direccion,
		    'npn' => $npn,
		    'comuna' => $comuna,
		    'barrio' => $objBarrioNombre,
		    'corregimiento' => $objCorregimientoNombre,
		    'vereda' => $objVeredaNombre,
		    'codigo' => $codigo,
		    'ip' => $this->consultarIp(),
		    'fecha' => date('Y-m-d H:i:s'),
		    'url' => $url
		];
	    }
	    return new Response($serializer->serialize($response, 'json'));
	} else {
	    return new Response($serializer->serialize($response, 'json'));
	}
    }

    /**
     * @Route("/usosuelo/guardarconsultaactividades")
     * @Method({"POST"})
     * @return Response 
     */
    public function guardarConsulta(Request $request) {

	$params = json_decode(file_get_contents('php://input'), true);
	$em = $this->getDoctrine()->getEntityManager(); //1. se debe llamar siempre el getEntityManager.
	$versionmapa = 0;

	$registroconsultausosuelo = new RegistroConsultaUsosuelo(); // se puede llamar directamente con el name space o lo podemos declarar en la parte de arriba para que quede seteado
	$registroconsultausosuelo->setPot2AreaActividad($em->getReference('saul\UsoSueloBundle\Entity\Pot2AreaActividad', $params['idAreaActividad']));
	$registroconsultausosuelo->setPot2Tipo($em->getReference('saul\UsoSueloBundle\Entity\Pot2Tipo', $params['idTipo']));
	$registroconsultausosuelo->setPot2Vocacion($em->getReference('saul\UsoSueloBundle\Entity\Pot2Vocacion', $params['idVocacion']));
	$registroconsultausosuelo->setIdcomuna($params['idComuna']);
	$registroconsultausosuelo->setIdbarrio($params['idBarrio']);
	$registroconsultausosuelo->setDireccion(preg_replace('/_/', ' ', $params['direccion']));
	$registroconsultausosuelo->setNpn($params['npn']);
	$registroconsultausosuelo->setCodciiu(trim($params['codCiiu']));
	$registroconsultausosuelo->setRespuesta(trim($params['respuesta']));
	$registroconsultausosuelo->setIp($this->consultarIp());
	$registroconsultausosuelo->setVersionmapa($versionmapa);
	$registroconsultausosuelo->setCodigo($params['codigo']);
	$registroconsultausosuelo->setCreatedAt(new \DateTime($params['fecha']));

	$useragent = $request->headers->get('User-Agent');
	$registroconsultausosuelo->setUseragent($useragent);

	$em->persist($registroconsultausosuelo); //2.persistir los datos seteados, guarda esos datos dentro de la entidad en Doctrine ORM.
	$flush = $em->flush(); //3.volcar o insertar todo lo que esta en la entidad ya en la tabla correspondiente a la base de datos.*/

	$response = [
	    'success' => true,
	    'msg' => 'Registro Guardado',
	    'data' => null
	];

	return new Response($this->get('serializer')->serialize($response, 'json'));
    }

    function generarCodigo($longitud) {
	$key = '';
	$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
	$max = strlen($pattern) - 1;
	for ($i = 0; $i < $longitud; $i++)
	    $key .= $pattern{mt_rand(0, $max)};
	return $key;
    }

    function consultarIp() {

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
    }

    function consultarUrl($area, $tipo, $vocacion) {
	$em = $this->getDoctrine()->getManager('pot_mc');
	$sql = "SELECT DISTINCT url1, url3, url5 FROM nur_areas_actividad_consulta WHERE id_area_ac = $area AND id_tipo = $tipo AND id_vocacio = $vocacion";
	//$sql = "SELECT DISTINCT url1, url3, url5 FROM nur_areas_actividad_consulta WHERE id_area_ac = 3 AND id_tipo = 4 AND id_vocacio = 12";
	$stmt = $em->getConnection()->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetchAll();
	$datos = null;

	foreach ($result as $value) {
	    $datos[] = array(
		'anexo' => $value['url1'],
		'norma' => $value['url3'],
		'resolucion' => $value['url5']
	    );
	}
	return $datos;
    }

    /**
     *
     * @Route("/usosuelo/consultarActividadesCiiu")
     * @Method({"GET"})
     */
    public function consultarActividadesCiiu() {

	$em = $this->getDoctrine()->getManager();
	$request = Request::createFromGlobals();

	$serializer = $this->get('serializer');
	$data = $request->query;
	$dataArray = array();

	$currentPage = $request->get('currentPage');
	$numPerPage = $request->get('numPerPage');
	$orderBy = 'codCiiu'; //$request->get('orderBy');	

	$resultodos = $em->getRepository('UsoSueloBundle:Pot2Ciiu')->consultarActividades($currentPage, $numPerPage, $orderBy, $dataArray);
	$response = ['alldata' => $resultodos['data']];

	return new Response($serializer->serialize($response, 'json'));
    }

    /**
     * @Route("/usosuelo/consultarespuesta")
     * @Method({"POST"})
     * 
     * @return Response
     */
    public function consultaRespuesta(Request $request) {

	$params = json_decode(file_get_contents('php://input'), true);
	$npn = $params['npn'];
	$direccion = $params['direccion'];
	$numactividades = $params['numactividades'];
	$actividades = $params['actividades'];

	$em = $this->getDoctrine()->getManager('idesc_mp');
	$sql = "SELECT direccion, the_geom FROM cov_base_direcciones_predios_urbanos WHERE direccion = '$direccion' ";
	$stmt = $em->getConnection()->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetchAll();

	//print_r($result);
	//die();

	$serializer = $this->get('serializer');
	if (count($result) > 0) {
	    $the_geomP = $result[0]['the_geom'];
	    if ($the_geomP) {
		$em = $this->getDoctrine()->getManager('pot_mc');
		$sql = "SELECT id_area_ac, id_tipo, id_vocacio FROM nur_areas_actividad_consulta WHERE intersects(the_geom, '$the_geomP')";
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll();

		if (count($result) > 0) {

		    $dataArray = array();
		    $dataArray['idareaactividad'] = $result[0]['id_area_ac'];
		    $dataArray['idtipo'] = $result[0]['id_tipo'];
		    $dataArray['idvocacion'] = $result[0]['id_vocacio'];
		    $dataArray['idCiiu'] = $actividades;
		    $em = $this->getDoctrine()->getManager();
		    $consultarRespuesta = $em->getRepository('UsoSueloBundle:Pot2Respuesta')->consultarRespuesta($this->get('saul.usodelsuelo'), $dataArray);

		    if (count($consultarRespuesta) > 0) {
			$response = ['data' => $consultarRespuesta, 'success' => true];
		    } else {
			$response = ['data' => $consultarRespuesta, 'success' => false];
		    }
		} else {
		    $response = ['data' => $result, 'success' => false];
		}
	    }
	} else {
	    $response = ['data' => $result, 'success' => false];
	}

	return new Response($serializer->serialize($response, 'json'));
    }

    /**
     * @Route("/usosuelo/imprimirpermitidos/{idAreaActividad}/{idTipo}/{idVocacion}/{codigo}/{npn}/{direccion}")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function imprimirRespuestaPermitidos(Request $request) {
	$dataArray = array();
	$dataArray['idareaactividad'] = $request->get('idAreaActividad');
	$dataArray['idtipo'] = $request->get('idTipo');
	$dataArray['idvocacion'] = $request->get('idVocacion');

	$em = $this->getDoctrine()->getManager();
	$consultarRespuesta = $em->getRepository('UsoSueloBundle:Pot2Respuesta')->consultarPermitidosImpresion($this->get('saul.usodelsuelo'), $dataArray);

	if (count($consultarRespuesta) > 0) {
	    $response = ['data' => $consultarRespuesta, 'codigo' => $request->get('codigo'), 'direccion' => $request->get('direccion'), 'npn' => $request->get('npn'), 'success' => true];
	    $this->generarPDFRespuestaPermitidos($response);
	}

	//$serializer = $this->get('serializer');
	//return new Response($serializer->serialize($response, 'json'));
    }

    public function generarPDFRespuestaPermitidos($data) {
	$em = $this->get('doctrine')->getManager();
	$pdf = new PlantillaRespuestaActividadesPermitidas('L', 'mm', 'A4', true, 'UTF-8', false, false, $this->container);
	$pdf->datos = $data['data'];
	$pdf->codigo = isset($data['codigo']) ? $data['codigo'] : '';
	$pdf->direccion = isset($data['direccion']) ? $data['direccion'] : '';
	$pdf->npn = isset($data['npn']) ? $data['npn'] : '';
	$pdf->parametrizarPdf();
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	$pdf->obtenerPlantilla();
    }

    /**
     * @Route("/usosuelo/verificaatvxpredio/{npn}")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function verificaATVxPredio($npn) {

	if (strlen($npn) == 30) {
	    $em = $this->get('doctrine')->getManager();
	    $sql = "SELECT direccion, npn, idareaactividad, idtipo, idvocacion FROM predioxareaxtipoxvocacion WHERE npn = '$npn' ";
	    $stmt = $em->getConnection()->prepare($sql);
	    $stmt->execute();
	    $result = $stmt->fetchAll();

	    if (count($result) > 0) {
		$dataArray['direccion'] = $result[0]['direccion'];
		$dataArray['npn'] = $result[0]['npn'];
		$dataArray['idareaactividad'] = $result[0]['idareaactividad'];
		$dataArray['idtipo'] = $result[0]['idtipo'];
		$dataArray['idvocacion'] = $result[0]['idvocacion'];

		$response = ['data' => $dataArray, 'success' => true];
	    } else {
		$response = ['data' => null, 'success' => false];
	    }
	} else {
	    $response = ['data' => null, 'success' => false];
	}

	return($response);
    }

    /**
     * @Route("/usosuelo/existeATV/{area}/{tipo}/{vocacion}")
     * @Method({"GET"})
     * 
     * @return Response
     */
    public function existeATV($area, $tipo, $vocacion) {

	$em = $this->get('doctrine')->getManager();
	$sql = "SELECT idareaactividad, idtipo, idvocacion FROM pot2_areaxtipoxvocacion WHERE idareaactividad = $area  and idtipo = $tipo and idvocacion = $vocacion";
	$stmt = $em->getConnection()->prepare($sql);
	$stmt->execute();
	$result = $stmt->fetchAll();

	if (count($result) > 0) {
	    return TRUE;
	} else {
	    return FALSE;
	}
    }

    /**
     * 
     * @param type $npn
     * @return boolean
     */
    public function esPredioZonaValida($idAreaActividad, $idTipo, $idVocacion, $npn) {
	if (strlen($npn) == 30) {
	    $predioverificado = $this->verificaATVxPredio($npn);
	    $idareactividadverificada = $predioverificado['data']['idareaactividad'];
	    $idtipoverificada = $predioverificado['data']['idtipo'];
	    $idvocacionverificada = $predioverificado['data']['idvocacion'];

	    if ($idareactividadverificada == $idAreaActividad && $idtipoverificada == $idTipo && $idvocacionverificada == $idVocacion) {
		return TRUE;
	    } else {
		return FALSE;
	    }
	} elseif ($npn == 'SIN INFORMACION PREDIAL') {
	    return TRUE;
	} elseif (strlen($npn) == 0) {
	    return($this->existeATV($idAreaActividad, $idTipo, $idVocacion));
	} else {
	    return FALSE;
	}
    }

}
